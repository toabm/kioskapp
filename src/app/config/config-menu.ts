import { ConfigMenuItem } from '../models/configMenuItem';
import {Constants} from "./constants";
import {nav_types} from "../models/appConfiguration";

export let configMenu: ConfigMenuItem[] = [
  {
    name: 'apiConfig',
    displayName: 'API URLs',
    iconName: 'server.svg',
    isGroup: true,
    description: "API Configuration",

    children: [
      {
        name: 'API_ENDPOINT',
        displayName: 'URL for production API',
        iconName: 'idleTimerConfig',
        isGroup: false,
        type: "text",
        defValue: Constants.defaultConfig.apiConfig.API_ENDPOINT,
        description: "URL for production API"
      },
      {
        name: 'API_MOCK_ENDPOINT',
        displayName: 'URL for demo APIs.',
        iconName: 'idleTimerConfig',
        isGroup: false,
        type: "text",
        defValue: Constants.defaultConfig.apiConfig.API_MOCK_ENDPOINT,
        description: "URL for demo API"
      },
    ]

  },
  {
    name: 'idleTimerConfig',
    displayName: 'Idle User Timer',
    iconName: 'timer.svg',
    isGroup: true,
    description: "Configuration for Idle User timeout",
    children: [
      {
        name: 'IDLE_TIMER',
        displayName: 'Idle Timer',
        isGroup: false,
        type: "number",
        defValue: Constants.defaultConfig.idleTimerConfig.IDLE_TIMER,
        description: "Time user can be idle before the log-out popup shows."
      },
      {
        name: 'IDLE_LOGOUT',
        displayName: 'Log-Out Timer',
        isGroup: false,
        type: "number",
        defValue: Constants.defaultConfig.idleTimerConfig.IDLE_LOGOUT,
        description: "Time the log-out popup stays on screen before the user is actually logged out."
      },
    ]
  },
  {
    name: 'alertsConfig',
    displayName: 'User\'s Alerts',
    iconName: 'warning.svg',
    isGroup: true,
    description: "Configuración for user\'s alerts and popups.",
    children: [
      {
        name: 'ALERT_TIMEOUT',
        displayName: 'Alert timeout',
        isGroup: false,
        type: "number",
        defValue: Constants.defaultConfig.alertsConfig.ALERT_TIMEOUT,
        description: "Time the alerts (i.e. \"Wrong Credentials...\") will show on screen."
      }
    ]
  },
  {
    name: 'labelsConfig',
    displayName: 'Sings & Labels',
    iconName: 'label.svg',
    isGroup: true,
    description: "App labels and texts...",
    children: [
      {
        name: 'STOCK_ES',
        displayName: 'Product on Stock',
        isGroup: false,
        type: "text",
        defValue: Constants.defaultConfig.labelsConfig.STOCK_ES,
        description: "Message to show in stocks balloons indicators for products (Cart section and Product section): Stock on current shop"
      },
      {
        name: 'STOCK_GLOBAL_ES',
        displayName: 'Product available throw shipping option',
        isGroup: false,
        type: "text",
        defValue: Constants.defaultConfig.labelsConfig.STOCK_GLOBAL_ES,
        description: "Message to show in stocks balloons indicators for products (Cart section and Product section): Can be sold but there is not stock on current shop."
      },
      {
        name: 'OUT_STOCK_ES',
        displayName: 'Product Out of Stock',
        isGroup: false,
        type: "text",
        defValue: Constants.defaultConfig.labelsConfig.OUT_STOCK_ES,
        description: "Message to show in stocks balloons indicators for products (Cart section and Product section): Out of Stock, product can't be sold."
      },
      {
        name: 'DELIVERY',
        displayName: 'Mark this item for delivery',
        isGroup: false,
        type: "text",
        defValue: Constants.defaultConfig.labelsConfig.DELIVERY,
        description: "Message to show on switches in payment section: Allow user to choose if picking up products on current shop, or ship them by post: User wants product to be sent by post."
      },
      {
        name: 'PICKUP',
        displayName: 'Mark this item to pick up on current shop',
        isGroup: false,
        type: "text",
        defValue: Constants.defaultConfig.labelsConfig.PICKUP,
        description: "Message to show on switches in payment section: Allow user to choose if picking up products on current shop, or ship them by post: User wants to pick up the product on current shop."
      },
    ]
  },
  {
    name: 'loginConfig',
    displayName: 'Login Configuration',
    iconName: 'security.svg',
    isGroup: true,
    description: "Configure Login options: No login, login throw credentials, login using QR code.",
    children: [
      {
        name: 'SHOW_LOGIN',
        displayName: 'Show/Hide login section.',
        isGroup: false,
        type: "checkbox",
        defValue: Constants.defaultConfig.loginConfig.SHOW_LOGIN,
        description: "Show/Hide login button on welcome screen to allow logged users."
      },
      {
        name: 'SHOW_FORM_LOGIN',
        displayName: 'Show/Hide credentials form login within login section.',
        isGroup: false,
        type: "checkbox",
        defValue: Constants.defaultConfig.loginConfig.SHOW_FORM_LOGIN,
        description: "Allow/Forbid access to credentials form within login section to allow users to login using user & password."
      },
      {
        name: 'SHOW_QR_LOGIN',
        displayName: 'Show/Hide QR login within login section.',
        isGroup: false,
        type: "checkbox",
        defValue: Constants.defaultConfig.loginConfig.SHOW_QR_LOGIN,
        description: "Allow/Forbid access to QR login section to allow users to use its QR code to login into de app."
      }
    ]
  },
  {
    name: 'navConfig',
    displayName: 'Product Navigation',
    iconName: 'navigation.svg',
    isGroup: true,
    description: "Configure options for products search and navigation.",
    children: [
      {
        name: 'NAV_TYPE',
        displayName: 'Choose navigation types.',
        isGroup: false,
        type: "select",
        defValue: Constants.defaultConfig.navConfig.NAV_TYPE,
        description: "Choose if default product navigation will be 1.Search by form, 2.Horizontal categories navigation or 3.Vertical categories navigation.",
        options: [{ val: nav_types.SEARCH, name:'Form text search'},{ val: nav_types.GRAPHIC_HORIZONTAL, name:'Graphic Horizontal Search.'}, { val: nav_types.GRAPHIC_VERTICAL, name:'Graphic Vertical Search'}]
      },
      {
        name: 'IMG_CATEGORY_CARDS',
        displayName: 'Show complex category cards with image.',
        isGroup: false,
        type: "checkbox",
        defValue: Constants.defaultConfig.navConfig.IMG_CATEGORY_CARDS,
        description: "Choose if complex cards with images included will be shown for each category, or simple cards with just the category name.",
      }
    ]
  },


];
