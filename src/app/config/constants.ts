// Angular Modules
import { Injectable } from '@angular/core';
import {AppConfigurationInterface, nav_types} from "../models/appConfiguration";
@Injectable({
  providedIn: 'root'
})
export class Constants {

  // Configuration default values: This values will by overridden by the values within config.json and by the /appConfiguration
  // endpoint in this order of priority.
  // Any properties not present in defaultConfig, even if they exist in config.json or in the corresponding API configuration
  // endpoint response, will be ignored.
  public static readonly defaultConfig: AppConfigurationInterface = {
    // API configuration
    apiConfig: {
      API_ENDPOINT: 'domain/api',                                                     // URL for production API
      API_MOCK_ENDPOINT: 'https://virtserver.swaggerhub.com/toabm/AQSKiosk/1.0.0default',     // URL for demo API
      API_CONFIG_ENDPOINT: "https://virtserver.swaggerhub.com/toabm/AQSKiosk/1.0.0" // URL for config endpoint (independent from server API
    },
    // Idle Timer configuration
    idleTimerConfig: {
      IDLE_TIMER:  180,                     // Time user can be idle before the log-out popup shows.
      IDLE_LOGOUT: 10                       // Time the log-out popup stays on screen before the user is actually logged out.
    },
    // User alerts configuration
    alertsConfig: {
      ALERT_TIMEOUT:  8                     // Time the alerts (i.e. "Wrong Credentials...") will show on screen.
    },
    // Labels configuration
    labelsConfig: {
      // Stock messages:
      STOCK_ES:  "EN STOCK",                // Message to show in stocks balloons indicators for products (Cart section and Product section): Stock on current shop
      STOCK_GLOBAL_ES: "TE LO ENVIAMOS",    // Message to show in stocks balloons indicators for products (Cart section and Product section): Can be sold but there is not stock on current shop.
      OUT_STOCK_ES: "NO DISPONIBLE",        // Message to show in stocks balloons indicators for products (Cart section and Product section): Out of Stock, product can't be sold.
      // Order labels
      DELIVERY: "ENVÍAMELO",                // Message to show on switches in payment section: Allow user to choose if picking up products on current shop, or ship them by post: User wants product to be sent by post.
      PICKUP: "ME LO LLEVO"                 // Message to show on switches in payment section: Allow user to choose if picking up products on current shop, or ship them by post: User wants to pick up the product on current shop.
    },
    // Access to sections
    loginConfig: {
      SHOW_LOGIN: true,                     // TRUE | FALSE --> Boolean to chose weather to show or not Login option in welcome screen.
      SHOW_FORM_LOGIN: true,                // TRUE | FALSE --> Boolean to chose weather to show or not credentials form Login option.
      SHOW_QR_LOGIN: true                   // TRUE | FALSE --> Boolean to chose weather to show or not QR Login option.
    },
    navConfig:  {
      NAV_TYPE: nav_types.GRAPHIC_VERTICAL,
      IMG_CATEGORY_CARDS: true              // Boolean to chose weather to show img cards for categories or simple cards without images.
    }
  }

  // List of Spanish provinces
  public static readonly PROVINCES: string[] = ['Álava/Araba','Albacete','Alicante','Almería','Asturias','Avila','Badajoz','Barcelona','Burgos','Cáceres',
    'Cádiz','Cantabria','Castellón','Ciudad Real','Córdoba','La Coruña/A Coruña','Cuenca','Gerona/Girona','Granada','Guadalajara',
    'Guipúzcoa/Gipuzkoa','Huelva','Huesca','Islas Baleares','Jaén','León','Lérida/Lleida','Lugo','Madrid','Málaga','Murcia','Navarra',
    'Orense/Ourense','Palencia','Las Palmas','Pontevedra','La Rioja','Salamanca','Segovia','Sevilla','Soria','Tarragona',
    'Santa Cruz de Tenerife','Teruel','Toledo','Valencia','Valladolid','Vizcaya/Bizkaia','Zamora','Zaragoza'];
}
