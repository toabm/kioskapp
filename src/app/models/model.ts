/**
 * The model will have several options before being particularized to a particular product. The modelDTO will have
 * to arrays with corresponding options:
 * + mainDimComponents -> Will specify the possibilities for the main dimension, normally color, it will be an array
 *      with the following shape: [{id: 5, name: "Black", code: "193"}, {...}, ...]
 * + dimensions --> Will specify any other possible dimension of this model, for instance, size: XS, S, M, L...
 *      dimensions: [{id: 1, name: color, dimComponents: [{id: 5, name: "Black", code: "193"}, {...}, ...]}, {...}]
 *
 *  mainDimComponents will be a dimension that, for the user, will have the meaning of different products, the rest
 *  of dimensions will have the meaning of same products, for example:
 *  mainDimComponents will be the color: For the user a blue t-shirt and a black t-shirt are different products.
 *  dimensions will contain the size: For the user blue S t-shirt and a blue XL t-shirt will be the same products.
 *
 *  The main dimension will be shown not only in the product page but also y the family products grid.
 *  The rest of dimensions will only be shown on the product page before adding to the cart.
 */
import {Dimension} from "./dimension";

export interface Model {
  id: string;
  reference: string;
  name: string;
  description: string;
  img_urls: string[];
  familyId: string;
  pvp: number | null;
  stock?: number;

  dimensions: Dimension[] | null;
  mainDimensionId?: string; // If this is present, the corresponding dimensions will be shown on models-grid.component.html
  mainDimension?: Dimension; // This will be filled by model-item.component.ts's ngOnInit, if mainDimensionId is present.
}


