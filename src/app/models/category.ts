export interface Category {
  id?: string
  name: string
  label: { es: string}
  description: string
  imgURL: URL
  parentCategoriesIds?: string[]
  orderPriority?: number
  isShown: boolean

  isSelected?: boolean
}
