import {TicketLine} from "./ticketLine";
import {Promo} from "./promo";

export interface Cart {
  ticketLines: Array<TicketLine>;
  promos?: Array<Promo>;     // Header promos, applied to whole cart.
  getTotalPVP(): number;            // The current order PVP. Calculated on front-end from products PVP, product quantities & promos.
  getLinePVP(ticketLine: TicketLine): number;
}
