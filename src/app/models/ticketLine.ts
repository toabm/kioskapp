import {Product} from "./product";
import {Promo} from "./promo";

export interface TicketLineInterface {
  id?: string;        // Ticket lines might be created from app so no id will be assigned.
  product: Product;
  quantity: number;
  promos?: Promo[];   // Line promos, applied only to current TicketLine. Promos will be applied by server.
  delivery?:boolean;  // Boolean to indicate if the product will be sent by delivery to corresponding address.
  getLinePVP?(): number;
}


export class TicketLine implements TicketLineInterface {

  id?: string;       // Ticket lines might be created from app so no id will be assigned.
  product: Product;
  quantity: number;
  promos?: Promo[];  // Line promos, applied only to current TicketLine. Promos will be applied by server.
  delivery?:boolean = false;

  constructor( ticketLine: TicketLineInterface) {
    // Object.assign has no type checking but the constructor checks that MyTicketLine if build from TicketLine interface.
    this.product = <Product>ticketLine.product;
    this.quantity = <number>ticketLine.quantity;
    this.delivery = <boolean>ticketLine.delivery;
    if (ticketLine.promos) this.promos = <Promo[]>ticketLine.promos;
  }

  public getLinePVP(): number {
    return this.promos?.length
      ? this.quantity * this.product.pvp - this.promos?.reduce(function(prev, cur) {return prev + cur.amount_vat}, 0)
      : this.quantity * this.product.pvp;
  }
}
