export interface SubFamily {
  id: string;
  name: string;
  familyId: string;
}
