import {Dimension} from "./dimension";

export interface Product {
  id: string;
  modelId: string;
  familyId: string;
  reference: string;
  name: string;
  description: string;
  img_urls: string[];
  pvp: number;
  stock?: number;
  stockGlobal?:number;
  dimensions?: Dimension[];
}
