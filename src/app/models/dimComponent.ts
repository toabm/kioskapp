export interface dimComponent {

  id: string;
  //dimId: string;
  name?: string;
  code?: string | null;
  imgURL?: string | null;
}
