import {Address} from "./address";

export interface User {
  id?: number;
  username: string;
  password: string;
  firstName?: string;
  lastName?: string;
  email?:string;
  token?: string;
  address?: Address;
}
