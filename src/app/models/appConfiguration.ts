/** Types definitions for configuration objects: Config parameters are grouped by types */

type ApiConfig = {
  [key: string]: string | undefined
  API_ENDPOINT: string      // URL for production API
  API_MOCK_ENDPOINT: string // URL for demo API
  API_CONFIG_ENDPOINT: string // URL for AQS Configuration Endpoint
}
type IdleTimerConfig = {
  [key: string]: number | undefined
  IDLE_TIMER: number        // Time user can be idle before the log-out popup shows.
  IDLE_LOGOUT: number       // Time the log-out popup stays on screen before the user is actually logged out.
}
type AlertsConfig = {
  [key: string]: number | undefined
  ALERT_TIMEOUT: number;    // Time the alerts (i.e. "Wrong Credentials...") will show on screen.
}
type LabelsConfig = {
  [key: string]: string | undefined
  STOCK_ES: string          // Message to show in stocks balloons indicators for products (Cart section and Product section): Stock on current shop
  STOCK_GLOBAL_ES: string   // Message to show in stocks balloons indicators for products (Cart section and Product section): Can be sold but there is not stock on current shop.
  OUT_STOCK_ES: string      // Message to show in stocks balloons indicators for products (Cart section and Product section): Out of Stock, product can't be sold.

  DELIVERY: string          // Message to show on switches in payment section: Allow user to choose if picking up products on current shop, or ship them by post: User wants product to be sent by post.
  PICKUP: string            // Message to show on switches in payment section: Allow user to choose if picking up products on current shop, or ship them by post: User wants to pick up the product on current shop.
}
type LoginConfig = {
  [key: string]: boolean | undefined
  SHOW_LOGIN: boolean;      // TRUE | FALSE --> Boolean to chose weather to show or not Login option in welcome screen.
  SHOW_FORM_LOGIN: boolean; // TRUE | FALSE --> Boolean to chose weather to show or not credentials form Login option.
  SHOW_QR_LOGIN: boolean;   // TRUE | FALSE --> Boolean to chose weather to show or not QR Login option.
}


export enum nav_types {
  SEARCH = "SEARCH",
  GRAPHIC_HORIZONTAL = "GRAPHIC_HORIZONTAL",
  GRAPHIC_VERTICAL ="GRAPHIC_VERTICAL"
}
type NavConfig = {
  [key: string]: string | boolean
  NAV_TYPE: nav_types
  IMG_CATEGORY_CARDS: boolean   // Boolean to chose weather to show img cards for categories or simple cards without images.
}


/**
 * Configuration object.
 */
export interface AppConfigurationInterface {
  [key: string]: any;
  apiConfig: ApiConfig
  idleTimerConfig: IdleTimerConfig
  alertsConfig: AlertsConfig
  labelsConfig: LabelsConfig
  loginConfig: LoginConfig
  navConfig: NavConfig
}


