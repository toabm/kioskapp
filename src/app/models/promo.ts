
export interface Promo {
  id?: string;
  lineId?: string;
  desc: string;
  amount: number;
  amount_vat: number;
  isHeaderPromo?: boolean;
}
