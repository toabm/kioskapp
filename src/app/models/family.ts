export interface Family {
  id: string;
  name: string;
  externalCode?: string | null;
}
