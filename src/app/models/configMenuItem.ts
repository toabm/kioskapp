export interface ConfigMenuItem {

  isGroup: boolean // Defines if the item represents a group of items or a particular property.

  // Common Properties, both for groups and end properties.
  name: string      // Group or Property name
  displayName: string
  description: string
  disabled?: boolean
  iconName?: string
  display?: boolean

  // Group attributes --> isGroup = true
  children?: ConfigMenuItem[]   // List of config properties.
  active?: boolean              // Is this group selected?

  // Config properties attributes (INPUTS) --> isGroup = false
  type?: "text" | "checkbox" | "number" | "radio" | 'select'
  options?: selectOptions[]
  defValue?: string | number | boolean
}

// Define options for navigation types.
interface selectOptions {
  val: string
  name: string
}
