import {dimComponent} from "./dimComponent";

export interface Dimension {
  id: string;
  name?: string;
  dimComponents: dimComponent[];
}
