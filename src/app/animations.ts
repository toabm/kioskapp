// ANIMATION DEFINITIONS
import {animate, style, transition, trigger} from "@angular/animations";

/**
 * This animation will be used across the whole app to create fadeIn fadeOut effects for different elements animating its opacity.
 * :enter and :leave are aliases for the void => * and * => void transitions.
 * */
const enterTransition = transition(':enter',
  [style({opacity: 0}), animate('.2s ease-in', style({opacity: 1}))]);
const leaveTrans = transition(':leave',
  [style({opacity: 1}), animate('.2s ease-out', style({opacity: 0}))]);
export const fadeIn = trigger('fadeIn', [enterTransition]);
export const fadeOut = trigger('fadeOut', [leaveTrans]);
