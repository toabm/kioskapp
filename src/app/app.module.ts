import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { SearchComponent } from './components/search/search.component';
import { ModelsGridComponent } from './components/models-grid/models-grid.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ModelItemComponent } from './components/models-grid/model-item/model-item.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { CartComponent } from './components/cart/cart.component';
import { CartItemComponent } from './components/cart/cart-item/cart-item.component';
import { CartSumupComponent } from './components/cart/cart-sumup/cart-sumup.component';
import { ZoomGalleryComponent } from './components/product-detail/zoom-gallery/zoom-gallery.component';
import { SwiperModule } from "swiper/angular";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { CheckoutTeaserComponent } from './components/product-detail/checkout-teaser/checkout-teaser.component';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from "@angular/common/http";
import {UserIdleModule} from "angular-user-idle";
import { IdleUserModalComponent } from './components/idle-user-modal/idle-user-modal.component';
import { AlertComponent } from './components/alert/alert.component';
import { LoginComponent } from './components/login/login.component';
import {JwtInterceptor} from "./api/jwy-interceptor.interceptor";
import {ErrorInterceptor} from "./api/error.interceptor";
import { LoginFormComponent } from './components/login/login-form/login-form.component';
import { LoginQRComponent } from './components/login/login-qr/login-qr.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner'; // QR Scanner
import { CheckoutComponent } from './components/checkout/checkout.component';
import {CommonModule} from "@angular/common";
import { AddressComponent } from './components/checkout/address/address.component';
import { TicketOptionsComponent } from './components/checkout/ticket-options/ticket-options.component';
import { ModalComponent } from './components/modal/modal.component';
import {AppConfigurationService} from "./services/app-configuration.service";
import {AppConfigurationInterface} from "./models/appConfiguration";
import {Observable, of, lastValueFrom} from "rxjs";
import {Constants} from "./config/constants";
import '../config.json';
import { ConfigComponent } from './components/configuration/config.component';
import { ConfigMenuItemComponent } from './components/configuration/config-menu-item/config-menu-item.component';
import { CheckboxComponent } from './components/_common/checkbox/checkbox.component';
import { GraphicNavigationComponent } from './components/graphic-navigation/graphic-navigation.component';
import { CategoryComponent } from './components/graphic-navigation/horizontal-category-nav/category/category.component';
import { CallbackPipe } from './components/_common/pipes/callback.pipe';
import { PixelButtonComponent } from './components/_common/buttons/pixel-button/pixel-button.component';
import { GlowButtonComponent } from './components/_common/buttons/glow-button/glow-button.component';
import { VerticalCategoryNavComponent } from './components/graphic-navigation/vertical-category-nav/vertical-category-nav.component';
import { HorizontalCategoryNavComponent } from './components/graphic-navigation/horizontal-category-nav/horizontal-category-nav.component';
import { CategoriesNavbarComponent } from './components/graphic-navigation/categories-navbar/categories-navbar.component';
import { CategoriesMapComponent } from './components/graphic-navigation/categories-map/categories-map.component';
import { InputDirective } from './directives/input.directive';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";

/**
 * This is a function that returns a function that returns a Promise<boolean>.
 * The promise function loads your configuration information and stores it in your application.
 * Once your configuration has been loaded, you resolve the promise using resolve(true).
 *
 * This method will load configuration parameters following this priority:
 *  + It will first retrieve parameters from config.json file --> This file must be edited in deployment with clients parameters (API URL).
 *  + It will then retrieve parameters from server --> Property apiConfig.API_ENDPOINT must have been configured on previous step.
 *  + All the parameter not found in the previous steps will be loaded from Constants.defaultConfig
 */
function loadConfig(http: HttpClient, appConfigurationService: AppConfigurationService): (() => Promise<boolean>) {
  return (): Promise<boolean> => {
    return new Promise<boolean>(( resolve: (a: boolean) => void, reject: (a: boolean) => void) => {
      // lastValueFrom takes as input an Observable and returns a Promise with the last emitted value, so we can use
      // promises' then() method to concat the async process and take the configuration properties in the correct order:
      // First we get config from Constants.defaultConfig-> Once completed we get values from config.json -> Once completed we retrieve values from database.
      lastValueFrom(of(Constants.defaultConfig) as Observable<AppConfigurationInterface>)
        .then((receivedConfig) =>
          appConfigurationService.mapConfigValues(receivedConfig as AppConfigurationInterface))
        .then(() =>
          lastValueFrom(http.get('./config.json') as Observable<AppConfigurationInterface>))
        .then((receivedConfig) =>
          appConfigurationService.mapConfigValues(receivedConfig as AppConfigurationInterface))
        .then(() =>
          lastValueFrom(appConfigurationService.getServerAppConfiguration()))
        .then((receivedConfig) =>
          appConfigurationService.mapConfigValues(receivedConfig as AppConfigurationInterface))
        .then( () =>
          resolve(true)); // After loading the 3 configuration sources we resolve the returned promise.
    });
  };
}

// Required for AOT (ahead of time) compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [AppComponent, WelcomeComponent, SearchComponent, ModelsGridComponent, NavBarComponent, ModelItemComponent, ProductDetailComponent, CartComponent, CartItemComponent, CartSumupComponent, ZoomGalleryComponent, CheckoutTeaserComponent, IdleUserModalComponent, AlertComponent, LoginComponent, LoginFormComponent, LoginQRComponent, CheckoutComponent, AddressComponent, TicketOptionsComponent, ModalComponent, ConfigComponent, ConfigMenuItemComponent, CheckboxComponent, GraphicNavigationComponent, CategoryComponent, CallbackPipe, PixelButtonComponent, GlowButtonComponent, VerticalCategoryNavComponent, HorizontalCategoryNavComponent, CategoriesNavbarComponent, CategoriesMapComponent, InputDirective],
  imports: [BrowserModule, CommonModule, AppRoutingModule, FormsModule, SwiperModule, ReactiveFormsModule,
    BrowserAnimationsModule, HttpClientModule, UserIdleModule, ZXingScannerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })],
  providers: [
    { // APP_INITIALIZER is a multi provider type that lets you specify a factory that returns a promise. When the promise completes, the application will continue on.
      provide: APP_INITIALIZER,
      // useFactory points to a function that returns a function that returns a Promise<boolean>
      useFactory: loadConfig,
      // This is because APP_INITIALIZER allows multiple instances of this provider. They all run simultaneously, but the code will not continue beyond APP_INTITIALIZER until all of the Promises have resolved.
      multi: true,
      // Configures the Injector to return an instance of a token.
      deps: [HttpClient, AppConfigurationService]
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [],

})
export class AppModule { }
