import { Location } from "@angular/common";
import { TestBed, fakeAsync, tick } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { Router } from "@angular/router";

import { routes } from "./app-routing.module";
import { AppComponent } from "./app.component";
import {SharedService} from "./services/shared.service";


describe("Router: App", () => {
  let location: Location;
  let router: Router;
  let fixture;

  // A component-under-test doesn't have to be injected with real services. In fact, it is usually better if they are
  // test doubles (stubs, fakes, spies, or mocks). The purpose of the spec is to test the component, not the service,
  // and real services can be trouble.
  let sharedServiceStub: Partial<SharedService>;
  sharedServiceStub = {emitEvent() { }};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      //declarations: [],
      providers: [{ provide: SharedService, useValue: sharedServiceStub }] // Inject sharedServiceStub, not real SharedService
    });

    // Grab a reference to the injected router.
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    // Create our root App component.
    fixture = TestBed.createComponent(AppComponent);
    // Sets up location change listener and perform initial navigation.
    router.initialNavigation();
  });

  it("fakeAsync works", fakeAsync(() => {
    let promise = new Promise(resolve => {setTimeout(resolve, 10)});
    let done = false;
    promise.then(() => (done = true));
    tick(50);
    expect(done).toBeTruthy();
  }));

  // Routing is an asynchronous activity so we need to use an async testing method: fakeAsync
  it('navigate to "" redirects you to /welcome', fakeAsync(() => {
    router.navigate([""]).then(() => {
      tick();
      expect(location.path()).toBe("/welcome");
    });
  }));

  // Routing is an asynchronous activity so we need to use an async testing method: fakeAsync
  it('navigate to "welcome" redirects you to /welcome', fakeAsync(() => {
    router.navigate(["welcome"]).then(() => {
      tick();
      expect(location.path()).toBe("/welcome");
    });
  }));

  it('navigate to "search" takes you to /search', fakeAsync(() => {
    router.navigate(["search"]).then(() => {
      tick();
      expect(location.path()).toBe("/search");
    });
  }));

  it('navigate to "products" takes you to /products', fakeAsync(() => {
    router.navigate(["products"]).then(() => {
      tick();
      expect(location.path()).toBe("/products");
    });
  }));
});
