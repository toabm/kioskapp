// Angular Modules
import { Injectable } from '@angular/core';
// Application Constants
import {environment} from "../../environments/environment";
import {AppConfigurationService} from "../services/app-configuration.service";

/**
 * This service can:
 *   + Create an API URL that uses the real or the mock API.
 *   + Create an API URL with query strings.
 *   + Create an API URL with path variables.
 *
 * All the API URLs will be provided from this service.
 */
@Injectable({providedIn: 'root'})export class ApiEndpointsService {

  constructor(private appConfig: AppConfigurationService) {}

  // CREATE URL
  private createUrl(action: string): string {
    return [ !environment.production
      ? this.appConfig.appConfigValues.apiConfig.API_MOCK_ENDPOINT
      : this.appConfig.appConfigValues.apiConfig.API_ENDPOINT, action].join('/');
  }


  // LOGIN
  public getLoginEndpoint(): string {
    return this.createUrl(`login`);
  }
  public getLoginQREndpoint(): string {
    return this.createUrl(`loginQR`);
  }

  // MODELS ENDPOINTS
  public getModelsEndpoint(): string {
    return this.createUrl(`models`);
  }

  public getModelsByCategoryEndpoint(categoryId:string): string {
    return this.createUrl(`models/category/${categoryId}`);
  }

  public getExistingDimensionsEndpoint(modelId: string): string {
    return this.createUrl(`model/${modelId}/dimensions`)
  }


  // PRODUCT ENDPOINTS
  public getProductByModelIdAndSelectedDimensionsEndpoint(modelId: string): string {
    return this.createUrl(`model/${modelId}/product`)
  }

  // FAMILY ENDPOINTS
  public getFamiliesEndpoint(): string {
    return this.createUrl(`families`);
  }
  public getSubFamiliesEndpointByFamilyId(familyId: string): string {
    return this.createUrl(`families/${familyId}/subFamilies`);
  }

  // CATEGORIES
  public getCategories(): string {
    return this.createUrl(`categories`);
  }


  // CART ENDPOINTS
  public evaluatePromosEndpoint(): string {
    return this.createUrl(`cart/promos`);
  }

  // CONFIGURATION ENDPOINTS
  public getAppConfigurationEndpoint() {
    return this.createUrl(`appConfiguration`);
  }
  public saveAppConfigurationEndpoint() {
    return this.createUrl(`appConfiguration`);
  }

}
