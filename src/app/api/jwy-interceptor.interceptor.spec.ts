import { TestBed } from '@angular/core/testing';

import { JwyInterceptorInterceptor } from './jwy-interceptor.interceptor';

describe('JwyInterceptorInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      JwyInterceptorInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: JwyInterceptorInterceptor = TestBed.inject(JwyInterceptorInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
