// Angular Modules
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";
@Injectable({providedIn: 'root'})
export class ApiHttpService {
  constructor(private http: HttpClient) { }

  public get<T>(url: string, options?: any): Observable<any> {
    return this.http.get(url, options);
  }
  public post<T>(url: string, data: any, options?: any): Observable<any> {
    return this.http.post(url, data, options);
  }
  public put(url: string, data: any, options?: any) {
    return this.http.put(url, data, options);
  }
  public delete(url: string, options?: any) {
    return this.http.delete(url, options);
  }
}
