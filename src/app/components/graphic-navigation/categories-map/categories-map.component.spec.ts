import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesMapComponent } from './categories-map.component';

describe('CategoriesMapComponent', () => {
  let component: CategoriesMapComponent;
  let fixture: ComponentFixture<CategoriesMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriesMapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
