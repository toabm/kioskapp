import {Component, EventEmitter, Input, Output} from '@angular/core';

import {fadeIn} from "../../../animations";
import {categoryClickEvent, navBar} from "../graphic-navigation.component";
import {Category} from "../../../models/category";
@Component({
  selector: 'app-categories-map',
  templateUrl: './categories-map.component.html',
  styleUrls: ['./categories-map.component.scss'],
  animations: [fadeIn]
})
export class CategoriesMapComponent {

  @Input("navBarsArray") navBarsArray: navBar[] | undefined;
  @Output("onCategorySelected") onCategorySelected: EventEmitter<categoryClickEvent> = new EventEmitter<categoryClickEvent>();

  constructor() { }

  clickedCategory($event:MouseEvent) {
    let target:Element = <Element>($event.target || $event.currentTarget || $event.srcElement);
    let event: categoryClickEvent = {categoryId: target.getAttribute("data-catid")!, navBarLevel: parseInt(target.getAttribute("data-level")!)};
    this.onCategorySelected.emit(event);
  }

  isThereAnySelectedCategory(): boolean {
    return !!this.navBarsArray!.map((navBar) => navBar.categories).reduce((a, b) => a.concat(b), []).find((cat: Category) => cat.isSelected);
  }
}
