import {Component,  OnInit} from '@angular/core';
import {Category} from "../../../models/category";
import {firstValueFrom, Observable} from 'rxjs';
import {ModelService} from "../../../services/model.service";
import {Model} from "../../../models/model";
import {Dimension} from "../../../models/dimension";
import {fadeIn, fadeOut} from "../../../animations";
import {AppConfigurationService} from "../../../services/app-configuration.service";
import {nav_types} from "../../../models/appConfiguration";
import {categoryClickEvent, navBar} from "../graphic-navigation.component";

@Component({
  selector: 'app-horizontal-category-nav',
  templateUrl: './horizontal-category-nav.component.html',
  styleUrls: ['./horizontal-category-nav.component.scss'],
  animations: [fadeIn, fadeOut]
})
export class HorizontalCategoryNavComponent implements OnInit {

  public categories: Category[] | undefined;
  public selectedCategoryId: string | undefined;

  public navBars:navBar[] | undefined;
  public categories_level0!: Category[];
  public categories_level1: Category[] | undefined;

  public modelsArray: Model[] | undefined;

  constructor(
    private modelService: ModelService,
    public configService: AppConfigurationService) { }


  // Get configuration constants
  get navTypes() {return nav_types;}

  async ngOnInit(): Promise<void> {

    const categories$: Observable<Category[]> = this.modelService.getCategories();
    this.categories = await firstValueFrom(categories$);
    this.categories_level0 = this.categories.filter(cat => cat.parentCategoriesIds!.length == 0 );
    this.navBars = [{categories:this.categories_level0, prevSelectedCat: ''}];
    return Promise.resolve();
  }


  categorySelected($event: categoryClickEvent) {

    let catId = $event.categoryId;

    // Remove all products from grid.
    this.modelsArray = [];

    let clickedCat: Category = this.categories!.find(cat => cat.id === catId)!;

    if(this.isLevelZeroCat(clickedCat)) { // If a level 0 category was clicked
      this.categories_level0!.concat(this.categories_level1 || []).filter(cat  => cat.isSelected).forEach(cat => cat.isSelected = false); // Set previous selection to false: both level 0 & level 1
      this.categories_level0!.find(cat  => cat.id == catId)!.isSelected = true; // Set current level 0 selected category.
      this.categories_level1 = this.categories!.filter(cat => cat.parentCategoriesIds?.includes(catId)); // Show corresponding level 1 categories.
    } else { // If a level 1 category was clicked.
      this.categories_level1!.filter(cat  => cat.isSelected).forEach(cat => cat.isSelected = false); // Set previous selection to false: Only under level 1 categories.
      this.categories_level1!.find(cat  => cat.id == catId)!.isSelected = true; // Set current level 1 selected category.
      this.navBars![1] = {categories:this.categories_level1!, prevSelectedCat: ''};
    }

    // Get models for selected category
    this.modelService.getModelsByCategoryId(catId).subscribe(models => {
      // Add model.mainDimension attribute for easier access.
      models.forEach( model => {if (model.mainDimensionId) model.mainDimension = model.dimensions?.find((dim: Dimension) => dim.id == model.mainDimensionId)})
      this.modelsArray = models;
    });
  }

  isLevelZeroCat(cat: Category) {
    return cat.parentCategoriesIds?.length === 0;
  }

  filterSelectedCategory(category: Category): boolean {
    return category.isSelected || false
  }
}
