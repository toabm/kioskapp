import {AfterViewInit, Component, ElementRef, Input, OnInit} from '@angular/core';
import {Category} from "../../../../models/category";
import {AppConfigurationService} from "../../../../services/app-configuration.service";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit, AfterViewInit {


  @Input("category") category: Category | undefined;
  @Input("index") index: number | undefined;

  colorArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
    '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
    '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
    '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
    '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
    '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
    '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
    '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
    '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];

  constructor(
    private elRef:ElementRef,
    public configService: AppConfigurationService) { }

  ngOnInit(): void {
    this.elRef.nativeElement.style.backgroundColor = this.colorArray[Math.floor(Math.random() * this.colorArray.length)];
  }

  ngAfterViewInit(): void {
    // We will set the card height from its SVG text using JS since it is an absolute positioned element.
    if (this.configService.appConfigValues.navConfig.IMG_CATEGORY_CARDS)
      this.elRef.nativeElement.style.width = `${this.elRef.nativeElement.querySelector('figcaption').clientWidth + 100}px`
  }

}
