import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HorizontalCategoryNavComponent } from './horizontal-category-nav.component';

describe('HorizontalCategoryNavComponent', () => {
  let component: HorizontalCategoryNavComponent;
  let fixture: ComponentFixture<HorizontalCategoryNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HorizontalCategoryNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HorizontalCategoryNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
