import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerticalCategoryNavComponent } from './vertical-category-nav.component';

describe('VerticalCategoryNavComponent', () => {
  let component: VerticalCategoryNavComponent;
  let fixture: ComponentFixture<VerticalCategoryNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerticalCategoryNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalCategoryNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
