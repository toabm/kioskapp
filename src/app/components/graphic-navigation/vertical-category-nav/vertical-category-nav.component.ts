import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Category} from "../../../models/category";
import {firstValueFrom, Observable} from "rxjs";
import {ModelService} from "../../../services/model.service";
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {Model} from "../../../models/model";
import {fadeIn} from "../../../animations";
import {Dimension} from "../../../models/dimension";
import {categoryClickEvent, navBar} from "../graphic-navigation.component";

// NavBar animations parameters
let slideInDuration: number = 300;
let waitBetweenAnimations: number = 100;

@Component({
  selector: 'app-vertical-category-nav',
  templateUrl: './vertical-category-nav.component.html',
  styleUrls: ['./vertical-category-nav.component.scss'],
  animations: [ fadeIn,
    trigger('slideInOut', [
      //transition(':enter, * => 1, * => 0', []),
      transition(':increment', [
        query('app-categories-navbar:enter', [
          style({ opacity: 0, width: 0, height: 0}),
          stagger(waitBetweenAnimations, [
            animate(`${slideInDuration}ms ease-out`, style({ opacity: 1, width: '*', height:'*' })),
          ]),
        ], { optional: true })
      ]),
      transition(':decrement', [
        query('app-categories-navbar:leave', [
          stagger(-waitBetweenAnimations, [
            animate(`${slideInDuration}ms ease-out`, style({ opacity: 0, width: 0 , height: 0})),
          ]),
        ]),
        query('app-categories-navbar:enter', [
          style({ opacity: 0, width: 0, height: 0}),
          stagger(waitBetweenAnimations, [
            animate(`${slideInDuration}ms ease-out`, style({ opacity: 1, width: '*', height:'*' })),
          ]),
        ], { optional: true })
      ]),
    ]),
  ]
})
export class VerticalCategoryNavComponent implements OnInit {

  public allCategories!: Category[];
  public navBars: navBar[] | undefined;
  public modelsArray: Model[] | [] =  [];

  @ViewChild('productsGrid') productsGrid?: ElementRef;

  constructor(private modelService: ModelService,
              private ref: ChangeDetectorRef) { }

  async ngOnInit(): Promise<void> {
    const categories$: Observable<Category[]> = this.modelService.getCategories();
    this.allCategories = await firstValueFrom(categories$);
    // Set first nav-bar with level 0 categories: categories with no parent category.
    this.navBars = [{categories: this.allCategories.filter(this.filterLevelOCategories), prevSelectedCat: ''}];
    return Promise.resolve();
  }




  filterLevelOCategories(category: Category): boolean {
    return category.parentCategoriesIds!.length == 0;
  }

  onNewCategorySelected($event: categoryClickEvent) {

    // Remove all products from grid.
    this.modelsArray = [];
    // NavBars amount
    let totalLevels = this.navBars!.length || 0;

    // Remove all nav bars below clicked nav bar.
    let removedItemsNumber: number = 0;
    if ($event.navBarLevel + 1 < totalLevels) {

      while ($event.navBarLevel as number + 1 < totalLevels && this.navBars!.length) {
        this.navBars!.pop();
        totalLevels = this.navBars!.length;
        removedItemsNumber++;
      }
      this.ref.detectChanges(); // Trigger animations
      this.ref.detach();        // Stop detecting changes, so we can finish data changes before UI is rendered.
    }
    // Set current selection on corresponding this.navBars item
    this.navBars!.find(navBar => navBar.categories.find(category => category.id == $event.categoryId))!.categories.forEach( cat => {
      cat.isSelected = cat.id == $event.categoryId;
    })

    // Get the sub-categories corresponding to current selected category.
    let childCategories: Category[] = this.allCategories.filter(cat => {return cat.parentCategoriesIds!.includes($event.categoryId)})
    if (childCategories.length) {
      childCategories.forEach(cat => cat.isSelected = false); // Reset previous selections
      this.navBars!.push({categories: childCategories, prevSelectedCat: $event.categoryId}); // Push a new nav bar with child categories.
    }

    // Delay detection of changes to sync animations
    if (removedItemsNumber > 0) setTimeout(() => this.ref.reattach(), slideInDuration + removedItemsNumber  + waitBetweenAnimations -100)

    // Get models for selected category
    this.modelService.getModelsByCategoryId($event.categoryId).subscribe(models => {
      // Add model.mainDimension attribute for easier access.
      models.forEach( model => {if (model.mainDimensionId) model.mainDimension = model.dimensions?.find((dim: Dimension) => dim.id == model.mainDimensionId)})
      this.modelsArray = models;
    });

    // adjust <app-graphic-navigation> height depending on the number of nav-bars.
    this.productsGrid!.nativeElement.style.height = `calc(${document.getElementsByTagName("app-graphic-navigation")[0].clientHeight
    - (document.getElementsByTagName("app-categories-navbar")[0].clientHeight + 8) * this.navBars!.length}px - 10rem)`
  }

  onAnimationEvent(event: any) { }

}
