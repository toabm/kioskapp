import {Component, ElementRef, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {Category} from "../../../models/category";
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";
import {categoryClickEvent} from "../graphic-navigation.component";


@Component({
  selector: 'app-categories-navbar',
  templateUrl: './categories-navbar.component.html',
  styleUrls: ['./categories-navbar.component.scss'],
  animations: [trigger('fadeInOut', [
    transition('* => *', [ // each time the binding value changes
      query(':leave', [
        stagger(100, [
          animate('0.5s 0.5s ease-out', style({ opacity: 0 }))
        ])
      ],{ optional: true }),
      query(':enter', [
        style({ opacity: 0 }),
        stagger(100, [
          animate('0.5s 0.5s ease-out', style({ opacity: 1 }))
        ])
      ], { optional: true })
    ])
  ])]

})
export class CategoriesNavbarComponent implements OnInit {

  // The categories to be shown on nav-bar.
  @Input("categories") categories!: Category[];
  @Input("level") level!: number;
  @Input("totalBars") totalBars!: number;
  // An event emitter to let parent component know when a category was clicked.
  @Output("onCategorySelected") public onCategorySelected: EventEmitter<categoryClickEvent> = new EventEmitter();
  public selectedCategoryId: string = '';

  // Bind fadeInOut to host to animate its span children, that is, the categories.
  @HostBinding('@fadeInOut') get fadeInOut() {
    return this.categories.length;
  }

  constructor(protected _ngEl: ElementRef) { }

  ngOnInit(): void {}

  public get element(): any {
    return this._ngEl.nativeElement;
  }

  clickedCategory(catId: string = '') {
    // We will not emit event if clicked category is in the last level and already selected.
    if (this.selectedCategoryId == catId && this.level == this.totalBars - 1) return
    this.selectedCategoryId = catId;
    this.onCategorySelected.emit({categoryId:catId, navBarLevel: this.level as number});
  }
}
