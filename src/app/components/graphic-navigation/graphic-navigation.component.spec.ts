import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphicNavigationComponent } from './graphic-navigation.component';

describe('GraphicNavigationComponent', () => {
  let component: GraphicNavigationComponent;
  let fixture: ComponentFixture<GraphicNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphicNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphicNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
