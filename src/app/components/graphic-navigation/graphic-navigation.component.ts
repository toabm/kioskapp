import {Component,  OnInit} from '@angular/core';
import {fadeIn, fadeOut} from "../../animations";
import {AppConfigurationService} from "../../services/app-configuration.service";
import {nav_types} from "../../models/appConfiguration";
import {Category} from "../../models/category";


export type navBar = { categories: Category[], prevSelectedCat: string};
export type categoryClickEvent = {categoryId: string, navBarLevel: number};

@Component({
  selector: 'app-graphic-navigation',
  templateUrl: './graphic-navigation.component.html',
  styleUrls: ['./graphic-navigation.component.scss'],
  animations: [fadeIn, fadeOut]
})
export class GraphicNavigationComponent implements OnInit {

  constructor(
    public configService: AppConfigurationService) { }

  // Get configuration constants
  get navTypes() {return nav_types;}

  async ngOnInit() {

  }


}
