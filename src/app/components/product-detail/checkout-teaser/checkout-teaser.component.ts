import {AfterViewChecked, Component, ElementRef, EventEmitter, HostBinding, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CartService} from "../../../services/cart.service";
import {animate, query, stagger, state, style, transition, trigger} from "@angular/animations";
import {SharedService} from "../../../services/shared.service";

// Animations
// Transition between open and closed states.
const openState = state('open', style({opacity: 1, height: '500px'}));
const closedState = state('closed', style({opacity: 0, height: 0}));
const teaserTransition = transition('void <=> *', [animate('0.5s ease-out')]);
const showHideAnimation = trigger('showHideAnimation', [openState, closedState, teaserTransition]);

// Transitions to apply to items list: fade in all elements with delay and animate last element's background-color.
const fadeInItemsTransition = transition(':enter', [
  query(':enter',
    [style({ opacity: 0 }), stagger('60ms', animate('600ms .2s ease-out', style({ opacity: 1 })))],
    { optional: true }),
  query('.item:last-child', [ animate('1.8s ease-out', style({ backgroundColor: '#34f734'}))], { optional: true })
]);
const fadeInItemsAnimation = trigger('fadeInItemsAnimation', [fadeInItemsTransition]);


@Component({
  host: {'(@showHideAnimation.done)': 'null'},
  selector: 'app-checkout-teaser',
  templateUrl: './checkout-teaser.component.html',
  styleUrls: ['./checkout-teaser.component.scss'],
  animations: [showHideAnimation, fadeInItemsAnimation]
})

export class CheckoutTeaserComponent implements OnInit, AfterViewChecked {

  @Input() isShown: boolean = false;
  @Output() close = new EventEmitter();

  @ViewChild('itemsList') private itemsList!: ElementRef;

  @HostBinding('@showHideAnimation') get getIsShown(): string {return this.isShown ? 'open' : 'closed';}

  // Close cart teaser on click outside the component. Except if we click the #addBtn button. No action if clicked inside.
  @HostListener('document:click', ['$event']) clickOut($event: MouseEvent) {
    let target:Element = <Element>($event.target || $event.currentTarget || $event.srcElement);
    if(this.eRef.nativeElement.contains(target)) { // Clicked inside
    } else if (this.isShown && target.getAttribute('id') !== 'addBtn') { this.isShown = false } // Clicked outside
    else if (target.getAttribute('id') === 'addBtn') this.isShown = true
  }

  constructor(public cartService: CartService, public sharedService: SharedService, private eRef: ElementRef) { }

  ngOnInit() {
    this.scrollToBottom();
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.itemsList.nativeElement.scrollTop = this.itemsList.nativeElement.scrollHeight;
    } catch(err) { }
  }
}
