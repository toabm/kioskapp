import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutTeaserComponent } from './checkout-teaser.component';

describe('CheckoutTeaserComponent', () => {
  let component: CheckoutTeaserComponent;
  let fixture: ComponentFixture<CheckoutTeaserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutTeaserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutTeaserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
