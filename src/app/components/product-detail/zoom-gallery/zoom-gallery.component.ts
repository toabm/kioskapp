import {Component, ViewEncapsulation, OnInit, Input, Output, EventEmitter, ViewChild} from "@angular/core";

// import Swiper core and required modules
import SwiperCore, { Navigation, Pagination } from "swiper";


// Install Swiper modules
SwiperCore.use([Navigation, Pagination]);

@Component({
  selector: 'zoom-gallery',
  templateUrl: './zoom-gallery.component.html',
  styleUrls: ['./zoom-gallery.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ZoomGalleryComponent implements OnInit {

  @Input() imgArray: string[] | undefined;
  @Input() startImg: number | undefined;
  @Output() close = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  onSwiper(swiper: any) {
    //console.log(swiper);
  }
  onSlideChange() {
    //console.log('slide change');
  }

}
