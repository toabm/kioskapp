import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomGalleryComponent } from './zoom-gallery.component';

describe('SwiperComponent', () => {
  let component: ZoomGalleryComponent;
  let fixture: ComponentFixture<ZoomGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoomGalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
