import {Component, OnInit, ViewChild} from '@angular/core';
import {Model} from "../../models/model";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {CartService} from "../../services/cart.service";
import {ModelService} from "../../services/model.service";
import {Dimension} from "../../models/dimension";
import {ProductService} from "../../services/product.service";
import {Product} from "../../models/product";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Constants} from "../../config/constants";
import {fadeIn, fadeOut} from "../../animations";
import {AppConfigurationService} from "../../services/app-configuration.service";
import {TranslateService} from "@ngx-translate/core";



// Animations for #addBtn
const canAddState = state('canAddState', style({opacity: 1, backgroundColor: 'green'}));
const cannotAddState = state('cannotAddState', style({opacity: .7, backgroundColor: '#999'}));
const buttonTransition = transition('canAddState <=> cannotAddState', [animate('0.5s')]);
const addBtnAnimation = trigger('addBtnAnimation', [canAddState, cannotAddState, buttonTransition]);

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
  animations: [fadeIn, fadeOut, addBtnAnimation]
})
export class ProductDetailComponent implements OnInit {

  model: Model;
  currProduct: Product | undefined;
  productUnavailable: boolean = false; // Var to define if the product is on stock from attributes stock and stockGlobal

  /** Vars for image-viewer component */
  galleryStartImgIndex: number = 0;
  viewerOpen:boolean = false;

  /** Dynamic text for addBtr */
  addBtnText: string | undefined;

  /** Variable to show/hide cart teaser when any product is added to CartService */
  showTeaser: boolean = false;
  teaserTimer?: number;

  // Get the dimensions form element: Both in ElementRef form and NgForm form.
  // @ViewChild('formElement') htmlForm: ElementRef | undefined;
  @ViewChild('dimensionsForm') dimensionsForm!: NgForm;

  constructor(private router: Router, private cartService: CartService, private modelService: ModelService,
              private productService: ProductService, public appConfigService: AppConfigurationService,
              private translate: TranslateService) {

    // If model comes within this.router.getCurrentNavigation()?.extras.state we will use that model, if not (page
    // has been reloaded) the this.router.getCurrentNavigation()?.extras.state will be lost and we will get the model
    // from sessionStorage.
    if (this.router.getCurrentNavigation()?.extras.state?.hasOwnProperty("params")) {
      this.model = this.router.getCurrentNavigation()?.extras.state?.params as Model;
      sessionStorage['model'] = JSON.stringify(this.model);
    } else {this.model = JSON.parse(sessionStorage['model'])}
  }

  ngOnInit(): void {
    // Set initial state for #addBtn
    if (this.model.dimensions?.length) this.addBtnText = `${this.translate.instant('productDetail.select')} ${this.model?.dimensions?.map(dim => dim.name).join(" & ")}`.toUpperCase();
    else this.getProduct();
    // this.showTeaser = true;
  }

  get constants() {return Constants;}

  /**
   *
   * @param $event
   * @param form
   */
  changeDimension($event: Event, form: NgForm) {

    // Reset current product in case there was one previously selected.
    this.currProduct = undefined;

    // The clicked input.
    let target:HTMLInputElement = <HTMLInputElement>($event.target || $event.currentTarget || $event.srcElement);
    let isInputDisabled = target.closest(".component")?.classList.contains("disabled")

    // If click on a disabled input, current combination of dimensions is not available, so we will reset the form
    if (isInputDisabled) {
      //$event.stopPropagation();
      // Clean from previous selections: Remove disabled, and selectable classes and uncheck checked radio buttons.
      document.querySelectorAll(".component, .selectable").forEach((el: Element) => {
        if (el.classList.contains('selectable')) el.classList.remove('selectable');
        if (el.classList.contains('disabled')) el.classList.remove('disabled');
      });
      this.dimensionsForm.resetForm(); // Reset the form since previous selection of dimensions does not exists.
      // Check the selected dimension component and, since the form is not valid (missing rest of dimensions components)
      // we will retrieve the existing combinations of dimensions for the newly selected value.
      target.dispatchEvent(new MouseEvent("click", {"view": window, "bubbles": true, "cancelable": false}))
    }

    if(!form.valid) { // Still some dimensions must be selected.

      // console.log("INVALID FORM!")

      // Get the dimensions that has not been selected to inform within the #addBtn
      let invalidDimsArray: String[] = [];
      for (let control in form.controls) {
        if (form.controls[control].status == "INVALID") invalidDimsArray.push(<String>document.getElementById(control)?.getAttribute("data-dimName"));
      }
      this.addBtnText = `${this.translate.instant('productDetail.select')} ${invalidDimsArray.join(" & ")}`

      // At first all dimComponents radio buttons will be enabled. When user select a dimComponent for any of the existing
      // dimensions, that is, clicks any radio button, we will retrieve from server the existing dimComponents for the rest
      // of the dimension.
      let selectedComponents: Dimension[] = this.getSelectedDimensions();
      let selectedComponentsIds: String[] = selectedComponents.map(dim => {return <String>dim.id}); // The dimensions that has been selected.

      // Retrieve possible dimensions combinations with currently selected options.
      this.modelService.getExistingDimensions(this.model.id, selectedComponents)
        .subscribe(dimensions => {

          // Filter the dimensions that have not been selected yet.
          let uncheckedDimensions = dimensions.filter( dim => !selectedComponentsIds.includes(dim.id.toString()));
          // For every non-selected dimension:
          for (const dim of uncheckedDimensions) {
            // Components belonging to current unselected dimension:
            let components = Array.from(document.querySelectorAll(`*[id^="dim_${dim.id}_dimComp"]`));
            // For each component of the non-selected dimension coming in the server answer (that is, selectable components),
            // we will add "selectable" class.
            for (const dimComp of dim.dimComponents) {
              let selectableComponent = document.querySelector(`input[id^="dim_${dim.id}_dimComp_${dimComp.id}"]`) as HTMLInputElement;
              selectableComponent.classList.add("selectable");
            }
            // The rest of the components (non-selectable) will be "disabled". Not actually disabled, just styled like disabled
            // since it can be clicked yet. In fact, a click on a disabled component will reset the whole form.
            components.forEach((el: Element) => {if (!el.classList.contains('selectable')) el.parentElement?.classList.add('disabled')});
          }
        });

    } else { // All dimensions have been selected.
      // Retrieve product info once we have selected a component for every dimension.
      this.getProduct();
    }
  }


  /**
   * Get the current selection of dimensions to either retrieve a product form server if all dimensions has one selected
   * dimComponent, or the existing combinations of dimensions in case not all dimensions has been selected.
   *
   * The form value will be something like:
   *
   * {dim_68: "1", ...} => Having 1 attribute for each dimension. In this example dimId = 68, dimComponentId = 1
   *
   * The dimensions will be retrieve as an array of Dimension objects, each item will contain one single dimComponent
   * corresponding with the user selection, that is:
   *
   * @returns [{id: selectedDimensionId, dimComponents: [{id: selectedDimComponentId}]}, ...]
   */
  getSelectedDimensions():Dimension[] {

    if (this.model.dimensions?.length == 0) return [];

    let selectedDimensions: Dimension[] = [];
    // Get the id of the dimensions that has already been assigned.
    for (const dimension in this.dimensionsForm.value) {
      const dimId = dimension.split("_")[1];
      const dimCompId = this.dimensionsForm.value[dimension];
      if (this.dimensionsForm.value[dimension] !== '' && this.dimensionsForm.value[dimension] !== null)
        selectedDimensions.push({id: dimId, name: <string>document.getElementById(`dim_${dimId}`)?.getAttribute("data-dimname"),
          dimComponents: [{id: this.dimensionsForm.value[dimension], name: <string>document.getElementById(`dim_${dimId}_dimComp_${dimCompId}`)?.getAttribute("data-compName")}]});
    }
    return selectedDimensions;
  }


  openZoomGallery (imgURL:string) {
    this.galleryStartImgIndex = this.model.img_urls.indexOf(imgURL)
    this.viewerOpen = true;
  }


  getProduct() {
    this.productService.getProductByModelIdAndDimensions(this.model.id, this.getSelectedDimensions()).subscribe(product => {
      this.currProduct = product;
      this.productUnavailable= (this.currProduct.stock == this.currProduct.stockGlobal ) && (this.currProduct.stock == 0);
      // Once the product has been correctly selected and retrieved from server we can enable the ADD TO CART Button:
      this.addBtnText = !this.productUnavailable ? this.translate.instant('productDetail.addToCart') : this.translate.instant('productDetail.notAvailable');
    });
  }

  /**
   * Add product to cart.
   * @param form
   */
  addProduct (form: any) {
    if(form.valid && this.currProduct && !this.productUnavailable) {
      this.cartService.addProductToCart(this.currProduct);
      this.showTeaser = true;
      if(this.teaserTimer) clearTimeout(this.teaserTimer);
      this.teaserTimer = setTimeout( () => this.showTeaser = false, 6000 );
    }
  }


}
