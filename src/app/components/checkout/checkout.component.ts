import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {fadeIn, fadeOut} from "../../animations";
import {CartService} from "../../services/cart.service";
import {Constants} from "../../config/constants";
import {ModalService} from "../../services/modal.service";
import {AppConfigurationService} from "../../services/app-configuration.service";


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  animations: [fadeIn, fadeOut]
})
export class CheckoutComponent implements OnInit, AfterViewInit {

  @Input() appContenteditableModel: string = "";

  evaluatedCart: Promise<boolean>;  // Our current cart evaluated on server side to include promos.
  isAddressNeeded: boolean = false; // If there is any line for delivery we need to show the address form

  constructor( public cartService: CartService, private modalService:ModalService, public appConfigService: AppConfigurationService) {
    // this.cartEvaluated with async pipe on html template will render TicketLines only once the cart's been evaluated for promos.
    this.evaluatedCart = this.cartService.evaluatePromos();
    // Once cart is received show/hide address form.
    this.evaluatedCart.then(() => {
      this.isAddressNeeded = this.cartService.ticketLines.filter(item => item.delivery).length > 0
    });

  }


  ngOnInit(): void { }

  ngAfterViewInit(): void {
  }


  // Get configuration constants
  get constants() {return Constants;}

  deliveryChange(prodId: string) {
    let input = document.getElementById(`toggleDelivery_${prodId}`) as HTMLInputElement;
    this.cartService.setDelivery(prodId, input.checked);
    this.isAddressNeeded = this.cartService.ticketLines.filter(item => item.delivery).length > 0
  }


  showPaymentModal() {
    this.modalService.open('paymentModal');
  }

}
