import { Component } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { Address } from 'src/app/models/address';
import {fadeIn, fadeOut} from "../../../animations";
import {AuthenticationService} from "../../../services/authentification.service";
import {Constants} from "../../../config/constants";

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  animations: [fadeIn, fadeOut]
})

export class AddressComponent {
  formInstance: FormGroup;

  constructor( public authenticationService: AuthenticationService) {
    this.formInstance = new FormGroup(
      {
        name_addr: new FormControl('', Validators.required),
        line1_addr: new FormControl('', Validators.required),
        line2_addr: new FormControl(''),
        town_addr: new FormControl('', Validators.required),
        city_addr: new FormControl('', Validators.required),
        zc_addr: new FormControl('', Validators.required),
        // country: new FormControl('', Validators.required),
      });

      if (this.authenticationService.isUserLoggedIn) {
        this.formInstance.patchValue({
          name_addr:  authenticationService.currentUserValue?.address?.name,
          line1_addr: authenticationService.currentUserValue?.address?.addressLine1,
          line2_addr: authenticationService.currentUserValue?.address?.addressLine2,
          town_addr:  authenticationService.currentUserValue?.address?.location,
          city_addr:  authenticationService.currentUserValue?.address?.city,
          zc_addr:    authenticationService.currentUserValue?.address?.zipCode
        });
      }


  }

  public get constants() {
    return Constants;
  }


  /**
   * This method close the active modal and pass the address object
   * to parent component.
   */
  save(){
    (Object.assign(new Address(), this.formInstance.value));
  }
}
