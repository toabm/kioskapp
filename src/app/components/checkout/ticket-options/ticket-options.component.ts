import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../../../services/authentification.service";

@Component({
  selector: 'app-ticket-options',
  templateUrl: './ticket-options.component.html',
  styleUrls: ['./ticket-options.component.scss']
})
export class TicketOptionsComponent implements OnInit {

  formTicketInstance: FormGroup;

  sentTicketByEmail: string = 'true';
  emailAddress?:string;

  constructor(public authenticationService: AuthenticationService) {

    this.formTicketInstance = new FormGroup(
      {
        email_select: new FormControl('', Validators.required),
        email_to: new FormControl('')
      });

    if (this.authenticationService.isUserLoggedIn) {
      this.formTicketInstance.patchValue({
        email_select:  true,
      });

      this.emailAddress = authenticationService.currentUserValue?.email;
    }
  }

  ngOnInit(): void {
  }

}
