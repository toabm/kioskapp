import { Component, OnInit } from '@angular/core';
import {CartService} from "../../../services/cart.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-cart-sumup',
  templateUrl: './cart-sumup.component.html',
  styleUrls: ['./cart-sumup.component.scss']
})
export class CartSumupComponent implements OnInit {

  promoCount: number = 0;
  totalAmountPromos: number = 0.00;

  constructor( public cartService: CartService,
               public _location: Location,) {
    // Calculate number of applied promos.
    this.promoCount = this.cartService.getAppliedPromosNumber();
    this.totalAmountPromos = this.cartService.getAppliedPromosAmount();
  }

  ngOnInit(): void {}

}
