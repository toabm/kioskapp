import {Component, OnInit} from '@angular/core';
import {CartService} from "../../services/cart.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  evaluatedCart: Promise<boolean>;

  constructor( public cartService: CartService ) {
    // this.cartEvaluated with async pipe on html template will render TicketLines only once the cart's been evaluated for promos.
    this.evaluatedCart = this.cartService.evaluatePromos();
    this.evaluatedCart.then( data => {
      // console.log(data)
      // console.log(this.cartService)
    })
  }

  ngOnInit(): void {}

}
