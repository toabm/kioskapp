import {Component, Input, OnInit} from '@angular/core';
import {CartService} from "../../../services/cart.service";
import { transition, style, animate, trigger } from '@angular/animations';
import { TicketLine} from "../../../models/ticketLine";
import {Constants} from "../../../config/constants";
import {AppConfigurationService} from "../../../services/app-configuration.service";

// https://stackblitz.com/edit/angular-anim-fade-in-out?file=src%2Fapp%2Fapp.component.ts
// https://angular.io/guide/transition-and-triggers

// :enter and :leave are aliases for the void => * and * => void transitions.
const enterTransition = transition(':enter', [
  style({opacity: 0}),
  animate('.4s ease-in', style({opacity: 1}))]);
const leaveTrans = transition(':leave', [
  style({opacity: 1}),
  animate('.4s ease-out', style({opacity: 0})),
  animate('.2s ease-out', style({opacity: 0, height: 0}))]);

const fadeIn  = trigger('fadeIn', [enterTransition]);
const fadeOut = trigger('fadeOut', [leaveTrans]);

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss'],
  animations: [fadeIn, fadeOut]
})
export class CartItemComponent implements OnInit {

  @Input("ticketLine") public ticketLine!: TicketLine;
  show = true;

  constructor( public cartService: CartService, public appConfigService: AppConfigurationService) {}

  ngOnInit(): void {}

  get constants() {
    return Constants;
  }

  incrementDecrementProduct($event: MouseEvent) {
    $event.preventDefault();

    // The clicked element.
    let target:Element = <Element>($event.target || $event.currentTarget || $event.srcElement);

    let fieldName = target.getAttribute('data-target');
    let parent = target.closest('.quantity-input-group');
    let input = parent?.querySelector(`input[name=${fieldName}]`) as HTMLInputElement;
    let currentVal = parseInt(input?.value, 10);

    if (target.classList.contains("button-plus")) {
      input.value = (currentVal+1).toString();
    } else if (target.classList.contains("button-minus")) {
      if (currentVal != 0) input.value = (currentVal-1).toString();
    } else input.value = '0';

  }
}
