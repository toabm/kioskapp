import { Component, OnInit } from '@angular/core';
import {ModelService} from "../../services/model.service";
import {Model} from "../../models/model";
import {Dimension} from "../../models/dimension";
import {Router} from "@angular/router";

@Component({
  selector: 'app-models-grid',
  templateUrl: './models-grid.component.html',
  styleUrls: ['./models-grid.component.scss']
})
export class ModelsGridComponent implements OnInit {


  modelsArray: Model[] | undefined;

  constructor(private router: Router, private modelService:ModelService) {

    let searchParams = this.router.getCurrentNavigation()?.extras.state?.params || {};

    console.log(searchParams)

    this.modelService.getModels(searchParams).subscribe(models => {
        // Add model.mainDimension attribute for easier access.
        models.forEach( model => {if (model.mainDimensionId) model.mainDimension = model.dimensions?.find((dim: Dimension) => dim.id == model.mainDimensionId)})
        this.modelsArray = models;
      });
  }

  ngOnInit(): void {

  }

}
