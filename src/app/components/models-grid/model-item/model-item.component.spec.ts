import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelItemComponent } from './model-item.component';
import {SharedService} from "../../../services/shared.service";
import {RouterTestingModule} from "@angular/router/testing";
import {routes} from "../../../app-routing.module";
import {CartService} from "../../../services/cart.service";
import {ActivatedRoute} from "@angular/router";

describe('ModelItemComponent', () => {
  let component: ModelItemComponent;
  let fixture: ComponentFixture<ModelItemComponent>;

  const fakeActivatedRoute = {
    snapshot: { data: { } }
  } as ActivatedRoute;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModelItemComponent ],
      providers: [CartService, SharedService, {provide: ActivatedRoute, useValue: fakeActivatedRoute}],
      imports: [RouterTestingModule.withRoutes(routes)]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
