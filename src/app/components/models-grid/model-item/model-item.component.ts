import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CartService} from "../../../services/cart.service";
import {Model} from "../../../models/model";
import {Dimension} from "../../../models/dimension";
import {SharedService} from "../../../services/shared.service";
import {TranslateService} from "@ngx-translate/core";



@Component({
  selector: 'app-model-item',
  templateUrl: './model-item.component.html',
  styleUrls: ['./model-item.component.scss']
})
export class ModelItemComponent implements OnInit {

  @Input() model: Model| undefined;
  //mainDimension: Dimension | undefined;

  constructor(
    // ActivatedRoute is specific to each component that the Angular Router loads.
    // ActivatedRoute contains information about the route and the route's parameters.
    private route: ActivatedRoute,
    // Inject cart service.
    private cartService: CartService,
    public sharedService: SharedService,
    public translate: TranslateService
  ) { }

  ngOnInit(): void {

    // Set main dimension.
    if (this.model) this.model.mainDimensionId = this.model.dimensions?.find((d:Dimension) => d.id === this.model?.mainDimensionId)?.id;
  }



  setMainPic($event: MouseEvent, img: string) {
    $event.stopPropagation();
    // The clicked element.
    let target:Element = <Element>($event.target || $event.currentTarget || $event.srcElement);

    let mainImg = target.closest(".photo-container")?.querySelector("#mainImg"); // Get corresponding #mainImg element.
    let prevImg = mainImg?.getAttribute("src"); // Img shown on clicked element.
    let newImg = target?.getAttribute("src"); // Img shown on #mainImg

    // Switch the img clicked by the image shown on #mainImg
    if (newImg != null)  { mainImg?.setAttribute("src", newImg);}
    if (prevImg != null) { target.setAttribute("src", prevImg);}


  }

  // TODO: Implement add producto to cart service.
  addToCart($event:MouseEvent, model: Model) {

    $event.stopPropagation();
    // this.cartService.addToCart(product);

    // window.alert('Your product has been added to the cart!');

    // Trigger Cart Animation
    this.cartService.triggerCartChanged();

  }

}
