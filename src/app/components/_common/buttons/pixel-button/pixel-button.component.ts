import {Component, Input, OnInit} from '@angular/core';

/**
 * Two types of pixel effect according to host data-type attribute:
 *    data-type = 'red'
 *    data-type = 'purple'
 */
@Component({
  selector: 'app-pixel-button',
  templateUrl: './pixel-button.component.html',
  styleUrls: ['./pixel-button.component.scss']
})
export class PixelButtonComponent implements OnInit {

  @Input("text") text: string | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
