import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-glow-button',
  templateUrl: './glow-button.component.html',
  styleUrls: ['./glow-button.component.scss']
})
export class GlowButtonComponent implements OnInit {


  @Input("text") text: string | undefined

  constructor() { }

  ngOnInit(): void {
  }

}
