import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input, OnChanges,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements  AfterViewInit, OnChanges {

  // The inner checkbox input
  @ViewChild('input') public input: ElementRef | undefined;

  // This variables will be mapped from <app-checkbox> parent attributes.
  @Input("isChecked") public isChecked:boolean = false;
  @Input("inputId")   public inputId:string = "";
  @Input("inputName") public inputName:string = "";
  @Input("label")     public label:string = "";
  @Input("disabled")  public disabled:boolean = false;

  // On input change we will emit an event to parent component <app-checkbox> so it will update corresponding property.
  @Output("onInputChange") public onInputChange = new EventEmitter<boolean>();

  constructor(private elementRef: ElementRef) {}

  /**
   * Here wil will map all dataset custom html attributes from <app-checkbox> parent element to its inner child <input>
   */
  ngAfterViewInit(): void {
    for (const [key, value] of Object.entries(this.elementRef.nativeElement.dataset)) {
      document.getElementById(this.inputId)!.dataset[key] = String(value);
    }
  }

  /**
   * This method will map data-old-vale attribute from <app-checkbox> parent element to <input> element since it will
   * be used to check if the property changed its value so it should be updated.
   * @param changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    if(this.elementRef.nativeElement.dataset && document.getElementById(this.inputId)) {
      for (const [key, value] of Object.entries(this.elementRef.nativeElement.dataset)) {
        document.getElementById(this.inputId)!.dataset[key] = String(value);
      }
    }
  }
}
