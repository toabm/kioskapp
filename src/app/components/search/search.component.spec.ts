import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { SearchComponent } from './search.component';
import {Router} from "@angular/router";
import {Location} from "@angular/common";
import {SharedService} from "../../services/shared.service";
import {RouterTestingModule} from "@angular/router/testing";
import {routes} from "../../app-routing.module";
import {By} from "@angular/platform-browser";

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


describe('Test SearchComponent clicks', () => {

  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let router: Router;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchComponent],
      providers: [{ provide: SharedService, useValue: undefined }],
      imports: [RouterTestingModule.withRoutes(routes)]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    // Grab a reference to the injected router.
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    // Sets up the location change listener and performs the initial navigation.
    router.initialNavigation();
  })

  it('should take us to /search on click on #searchBtn', fakeAsync(() => {
    let searchButtonElement = fixture.debugElement.query(By.css('#searchBtn'));
    searchButtonElement.triggerEventHandler('click', null);
    tick();
    expect(location.path()).toBe("/products");
  }));
});
