import {Component, OnInit, ViewChild} from '@angular/core';
import {ModelService} from "../../services/model.service";
import {Family} from "../../models/family";
import {SubFamily} from "../../models/subFamily";
import {SharedService} from "../../services/shared.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  // List of possible categories.
  public families:Family[] | undefined;
  // Selected category on UI
  public selectedFamily:string = '';

  // List of possible categories.
  public subFamilies: SubFamily[] | undefined;
  // Selected category on UI
  public selectedSubfamily:String = '';

  @ViewChild('searchModelForm') searchModelForm!: NgForm;


  /**
   * Inject product service on constructor.
   * @param sharedService
   * @param modelService
   */
  constructor( public sharedService: SharedService, private modelService:ModelService) { }

  ngOnInit(): void {

    this.modelService.getFamilies()
      .subscribe((families: Family[]) => this.families = families);

  }


  /**
   *
   * @param newValue
   */
  onFamilyChange(newValue:string) {
    this.selectedFamily = newValue;
    // Get corresponding sub-families:
    this.modelService.getSubFamilies(this.selectedFamily)
      .subscribe(subFamilies => this.subFamilies = subFamilies);
  }


  searchModels() {
    document.getElementById("searchBtn")!.classList.add("active");
    setTimeout( () => this.sharedService.goToPage('products', this.searchModelForm.value), 1000);

  }

}
