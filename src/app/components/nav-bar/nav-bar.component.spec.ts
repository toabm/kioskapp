import {ComponentFixture, TestBed, fakeAsync, tick} from '@angular/core/testing';
import {SharedService} from "../../services/shared.service";
import { NavBarComponent } from './nav-bar.component';
import {WelcomeComponent} from "../welcome/welcome.component";
import {SearchComponent} from "../search/search.component";
import { Location } from "@angular/common";
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../../app-routing.module';
import {AppComponent} from "../../app.component";
import {Router} from "@angular/router";
import {By} from "@angular/platform-browser";

// A component-under-test doesn't have to be injected with real services. In fact, it is usually better if they are
// test doubles (stubs, fakes, spies, or mocks). The purpose of the spec is to test the component, not the service,
// and real services can be trouble.
let sharedServiceStub: Partial<SharedService>;
sharedServiceStub = {
  emitEvent(change: any) {
    //this.emitChangeSource.next(change)
  }
};


describe('Should render NavBarComponent always except on /welcome url', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let router: Router;
  let location: Location;



  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavBarComponent, SearchComponent, WelcomeComponent, AppComponent ],
      providers: [SharedService], // Inject sharedServiceStub, not real SharedService
      imports: [RouterTestingModule.withRoutes(routes)]
    }).compileComponents();
  });

  beforeEach(() => {

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    // Grab a reference to the injected router.
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);

    // Sets up the location change listener and performs the initial navigation.
    router.initialNavigation();

    // Trigger a change detection cycle for the component.
    fixture.detectChanges();
  });

  it('should create', () => {expect(component).toBeTruthy()});

  it("fakeAsync works", fakeAsync(() => {
    let promise = new Promise(resolve => {setTimeout(resolve, 10)});
    let done = false;
    promise.then(() => (done = true));
    tick(50);
    expect(done).toBeTruthy();
  }));


  it('should not render when WelcomeComponent is shown', fakeAsync(() => {
    router.navigate(["welcome"]).then(() => {
      // Simulates the asynchronous passage of time for the timers in the fakeAsync zone.
      tick();
      expect(location.path()).toBe("/welcome");
      // Tell the TestBed to perform data binding by calling fixture.detectChanges().
      // Delayed change detection is intentional and useful. It gives the tester an opportunity to inspect and change
      // the state of the component before Angular initiates data binding and calls lifecycle hooks
      fixture.detectChanges();
      // nav-bar should not be rendered
      expect(fixture.debugElement.query(By.css('#nav-bar'))).toBeNull();
      expect(fixture.debugElement.query(By.css('#nav-bar'))).toBeFalsy();
    });
  }));


  it('should render when SearchComponent is shown', fakeAsync(() => {
    router.navigate(["search"]).then(() => {
      tick();
      expect(location.path()).toBe("/search");
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#nav-bar')).nativeElement).toBeTruthy();
    });
  }));

  it('should render when ProductsComponent is shown', fakeAsync(() => {
    router.navigate(["products"]).then(() => {
      tick();
      expect(location.path()).toBe("/products");
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('#nav-bar')).nativeElement).toBeTruthy();
    });
  }));



});


describe('Test NavBar inputs', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let navBarComponent: NavBarComponent;
  let router: Router;
  let location: Location;

  beforeEach(async ()  => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent, NavBarComponent],
      providers: [SharedService], // Inject sharedServiceStub, not real SharedService
      imports: [RouterTestingModule.withRoutes(routes)]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    // Grab a reference to the injected router.
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);

    // Sets up the location change listener and performs the initial navigation.
    router.initialNavigation();

    // get the child component instance
    navBarComponent = fixture.debugElement.query(By.css('app-nav-bar')).componentInstance as NavBarComponent;
    fixture.detectChanges(); // Trigger a change detection cycle for the component.
  })

  it('should set showNavBar to the false when /welcome', fakeAsync(() => {
    router.navigate(["welcome"]).then(() => {
      tick();
      expect(location.path()).toBe("/welcome");
      fixture.detectChanges();
      expect(navBarComponent.showNavBar).toBe(false);
    });
  }));

  it('should set showNavBar to the true when /products', fakeAsync(() => {
    router.navigate(["products"]).then(() => {
      tick();
      expect(location.path()).toBe("/products");
      fixture.detectChanges();
      expect(navBarComponent.showNavBar).toBe(true);
    });
  }));
});



describe('Test NavBar elements clicks', () => {

  let component: NavBarComponent;
  let fixture: ComponentFixture<NavBarComponent>;
  let router: Router;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavBarComponent],
      providers: [SharedService], // Inject sharedServiceStub, not real SharedService
      imports: [RouterTestingModule.withRoutes(routes)]
    }).compileComponents();

    fixture = TestBed.createComponent(NavBarComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    // Grab a reference to the injected router.
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);

    // Sets up the location change listener and performs the initial navigation.
    router.initialNavigation();
  })

  it('should click home button and take us to /welcome', fakeAsync(() => {

    fixture.detectChanges();
    router.navigate(["products"]).then(() => {
      let homeButtonElement = fixture.debugElement.query(By.css('#home'));
      expect(location.path()).toBe("/products");
      homeButtonElement.triggerEventHandler('click', null);
      tick();
      fixture.detectChanges();
      expect(location.path()).toBe("/welcome");
    });
  }));
});
