import {Component, ElementRef, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {SharedService} from "../../services/shared.service";
import {NavigationStart, Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentification.service";
import {AppConfigurationService} from "../../services/app-configuration.service";
import {CartService} from "../../services/cart.service";



@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {

  public showNavBar: boolean = true;

  // Get cart button child to apply animations.
  @ViewChild('cart') cartBtn: ElementRef | undefined;

  constructor(
    private _location: Location,
    private _sharedService: SharedService,
    private router: Router,
    public authenticationService: AuthenticationService,
    public configService: AppConfigurationService,
    public cartService: CartService) {

    // Receive events emitted by SharedService
    _sharedService.changeEmitted.subscribe(
      topic => {
        topic === "cartChanged" ? this.triggerCartAnimation()  : null;
      });

    this.hideNavBarComponentWhenWelcomeComponentShown();
  }


  backClicked() {
    this._location.back();
  }

  /**
   * Listen to router events to hide nav-bar in case we are showing welcome component.
   */
  hideNavBarComponentWhenWelcomeComponentShown = () => {
    this.router.events.forEach((event) => {
      // Types of events: [NavigationStart, NavigationEnd, NavigationCancel, NavigationError, RoutesRecognized]
      if(event instanceof NavigationStart) {
        event.url === "/welcome" ? this.showNavBar = false : this.showNavBar = true ;
      }
    });
  }


  updateCartItemsMarker(count: string) {
    this.cartBtn!.nativeElement.setAttribute('data-count', count);
  }

  triggerCartAnimation() {
    let $cartBtn = this.cartBtn!.nativeElement;
    let afterAnimation = () => {
      $cartBtn.classList.remove('shaking');
      this._sharedService.removeMultipleEventListeners($cartBtn, "animationend webkitAnimationEnd", afterAnimation);
    }
    this._sharedService.addMultipleEventListeners($cartBtn, "animationend webkitAnimationEnd", afterAnimation)
    $cartBtn.addEventListener("animationend webkitAnimationEnd", afterAnimation);
    $cartBtn.classList.add('shaking'); // Trigger CSS animation
    this.updateCartItemsMarker(this.cartService.getTotalItems().toString());
  }


}
