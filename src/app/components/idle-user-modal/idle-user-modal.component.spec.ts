import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdleUserModalComponent } from './idle-user-modal.component';

describe('IdleUserModalComponent', () => {
  let component: IdleUserModalComponent;
  let fixture: ComponentFixture<IdleUserModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdleUserModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdleUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
