import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SharedService} from "../../services/shared.service";
import {AuthenticationService} from "../../services/authentification.service";

@Component({
  selector: 'app-idle-user-modal',
  templateUrl: './idle-user-modal.component.html',
  styleUrls: ['./idle-user-modal.component.scss']
})
export class IdleUserModalComponent implements OnInit {


  @Output() close = new EventEmitter();

  constructor( public sharedService: SharedService, public authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }


  keepShopping():void {
    this.close.emit();
  }

  doLogout() {
    this.sharedService.stopIdleTimer();
    this.authenticationService.logout("welcome");
  }
}
