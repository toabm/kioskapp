import {Component, OnDestroy, OnInit} from '@angular/core';
import {SharedService} from "../../services/shared.service";
import {AuthenticationService} from "../../services/authentification.service";
import {AppConfigurationService} from "../../services/app-configuration.service";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit, OnDestroy {

  constructor(
    public _sharedService: SharedService,
    private authenticationService: AuthenticationService,
    public configService: AppConfigurationService) {
  }

  ngOnInit(): void {
    this._sharedService.stopIdleTimer();
    this.authenticationService.logout();
  }

  loginAccess():void {
    this._sharedService.goToPage('login');
  }

  directAccess(): void {
    this.configService.appConfigValues.navConfig.NAV_TYPE === 'SEARCH'
      ? this._sharedService.goToPage('search')
      : this._sharedService.goToPage('graphicNavigation')
  }

  ngOnDestroy(): void {
    this._sharedService.startIdleTimer();
  }
}
