import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {SharedService} from "../../services/shared.service";
import { WelcomeComponent } from './welcome.component';
import {Router} from "@angular/router";
import {Location} from "@angular/common";
import {routes} from "../../app-routing.module";
import {By} from "@angular/platform-browser";

describe('WelcomeComponent', () => {
  let component: WelcomeComponent;
  let fixture: ComponentFixture<WelcomeComponent>;

  // A component-under-test doesn't have to be injected with real services. In fact, it is usually better if they are
  // test doubles (stubs, fakes, spies, or mocks). The purpose of the spec is to test the component, not the service,
  // and real services can be trouble.
  let sharedServiceStub: Partial<SharedService>;
  sharedServiceStub = {emitEvent(change: any) { }};


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WelcomeComponent ],
      imports: [RouterTestingModule],
      providers: [{ provide: SharedService, useValue: sharedServiceStub }] // Inject sharedServiceStub, not real SharedService
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {expect(component).toBeTruthy()});
});


describe('Test WelcomeComponent clicks', () => {

  let component: WelcomeComponent;
  let fixture: ComponentFixture<WelcomeComponent>;
  let router: Router;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WelcomeComponent],
      providers: [{ provide: SharedService, useValue: undefined }],
      imports: [RouterTestingModule.withRoutes(routes)]
    }).compileComponents();

    fixture = TestBed.createComponent(WelcomeComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    // Grab a reference to the injected router.
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    // Sets up the location change listener and performs the initial navigation.
    router.initialNavigation();
  })

  it('should take us to /search on click on #searchBtn', fakeAsync(() => {
    fixture.detectChanges();
    let searchButtonElement = fixture.debugElement.query(By.css('#searchBtn'));
    expect(location.path()).toBe("");
    searchButtonElement.triggerEventHandler('click', null);
    tick();
    fixture.detectChanges();
    expect(location.path()).toBe("/search");
  }));
});
