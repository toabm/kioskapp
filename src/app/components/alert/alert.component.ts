import { Component, OnInit, OnDestroy} from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from '../../services/alert.service';
import { fadeIn, fadeOut } from "../../animations";
import {Constants} from "../../config/constants";
import {AppConfigurationService} from "../../services/app-configuration.service";

@Component({
  selector: 'app-alert',
  templateUrl: 'alert.component.html',
  styleUrls: ['alert.component.scss'],
  animations: [fadeIn, fadeOut]
})
export class AlertComponent implements OnInit, OnDestroy {

  private subscription?: Subscription;
  private timer?: number;
  message: any;

  constructor(private alertService: AlertService, private appConfigService: AppConfigurationService) {}

  ngOnInit() {
    this.subscription = this.alertService.getAlert$()
      .subscribe(this.newAlertReceived);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }


  /**
   * next() method for getAlert$() Observable from AlertService.
   *
   * This method will be called everytime .next({type:'type', text: text'}) method is called on getAlert$() Observable.
   *
   * @param message
   */
  newAlertReceived = async (message: any) => {

    // If a new alert is received while previous one is already showing we will reset timer.
    if (this.timer) clearTimeout(this.timer);

    // Set the alert styles: success or error.
    switch (message && message.type) {
      case 'success':
        message.cssClass = 'alert alert-success';
        break;
      case 'error':
        message.cssClass = 'alert alert-error';
        break;
    }
    // By setting the message the alert div will be shown on UI.
    this.message = message;

    // Close alert div after Constants.ALERT_TIMEOUT seconds.
    this.timer = setTimeout(() => {
      this.message = null;
    }, this.appConfigService.appConfigValues.alertsConfig.ALERT_TIMEOUT * 1000);

  }

  // Close modal: remove message content and alert modal will be hidden.
  close(): void {
    this.message = null;
  }


}
