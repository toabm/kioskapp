import { Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../../services/authentification.service";
import {AlertService} from "../../../services/alert.service";
import {first} from "rxjs/operators";
import {capitalizeFirstLetter} from "../../../services/shared.service";
import {fadeIn, fadeOut} from "../../../animations";
import {AppConfigurationService} from "../../../services/app-configuration.service";
import {KeyboardService} from "../../../services/keyboard.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  animations: [fadeIn, fadeOut]
})
export class LoginFormComponent implements OnInit {

  loginForm!: FormGroup;
  loading = false;
  submitted = false;
  returnUrl!: string;


  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private configService: AppConfigurationService,
    public keyboardService: KeyboardService,
    private translate: TranslateService
  ) {  }

  ngOnInit(): void {

    // Configurate login form fields
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // Get return url from route parameters or default to '/search'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || this.configService.appConfigValues.navConfig.NAV_TYPE === 'SEARCH' ? 'search' : 'graphicNavigation';
  }



  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  // Call API login service on form submit
  onSubmit() {
    this.submitted = true;
    // Reset alerts on submit
    this.alertService.clear();
    // Stop here if form is invalid
    if (this.loginForm.invalid) {
      this.alertService.error(this.translate.instant("login.errorsInFormWarning"));
      return;
    }
    // Show spin loader on LOGIN Button
    this.loading = true;
    // Do login using AuthenticationService
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first()) // The call to .pipe(first()) unsubscribes from the observable immediately after the first value is emitted.
      .subscribe(
        (data:any) => {
          this.alertService.success(`Bienvenido ${capitalizeFirstLetter([...this.authenticationService.currentUserValue?.username!])}`, true);
          this.router.navigate([this.returnUrl]);
        },
        (error:any) => {
          this.authenticationService.logout();
          this.alertService.error("Error de logueo: Por favor, compruebe sus credenciales...");
          this.loading = false;
        });
  }

}

