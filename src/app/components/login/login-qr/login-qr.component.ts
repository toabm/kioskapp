import { Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../../services/authentification.service";
import {AlertService} from "../../../services/alert.service";
import {BarcodeFormat} from "@zxing/library";
import {ZXingScannerComponent} from "@zxing/ngx-scanner";
import {first} from "rxjs/operators";
import {BehaviorSubject} from "rxjs";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-login-qr',
  templateUrl: './login-qr.component.html',
  styleUrls: ['./login-qr.component.scss']
})
export class LoginQRComponent implements OnInit {

  loginForm!: FormGroup;
  loading = false;
  submitted = false;
  returnUrl!: string;

  allowedQRFormats: BarcodeFormat[] = [ BarcodeFormat.QR_CODE, BarcodeFormat.EAN_13, BarcodeFormat.CODE_128, BarcodeFormat.DATA_MATRIX /*, ...*/ ];
  @ViewChild('scanner')
  scanner: ZXingScannerComponent | undefined;


  availableDevices?: MediaDeviceInfo[];
  currentDevice: MediaDeviceInfo | undefined;

  hasDevices: boolean = false;
  hasPermission: boolean = false;

  qrResultString: string | null = null;

  torchEnabled = false;
  torchAvailable$ = new BehaviorSubject<boolean>(false);
  tryHarder = false;

  constructor(    private formBuilder: FormBuilder,
                  private route: ActivatedRoute,
                  private router: Router,
                  private authenticationService: AuthenticationService,
                  private alertService: AlertService,
                  private translate: TranslateService) {

  }

  ngOnInit(): void {
    // Configurate login form fields
    this.loginForm = this.formBuilder.group({qr_code: ['', Validators.required]});
    // Get return url from route parameters or default to '/search'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/search';
  }

  doLogin(qrCode: string) {
    this.submitted = true;
    // Reset alerts on submit
    this.alertService.clear();
    // Show spin loader on LOGIN Button
    this.loading = true;
    // Do login using AuthenticationService
    this.authenticationService.loginQR(qrCode)
       .pipe(first()) // The call to .pipe(first()) unsubscribes from the observable immediately after the first value is emitted.
       .subscribe(
         (data:any) => {
           this.alertService.success(`Bienvenido ${this.authenticationService.currentUserValue?.username!}`, true);
           this.router.navigate([this.returnUrl]);
         },
         (error:any) => {
           this.authenticationService.logout();
           this.alertService.error("Error de logueo: Por favor, compruebe sus credenciales...");
           this.loading = false;
         });
  }



  clearResult(): void {
    this.qrResultString = null;
  }

  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
    document.getElementById("redBar")?.classList.add("animate");
  }

  onCamerasNotFound(): void {
    this.alertService.error(this.translate.instant('login.noQRReaderWarning'), true );
    setTimeout( () => document.getElementById("showForm")?.click(), 3000);
  }

  onDeviceSelectChange(selected: string) {
    const device = this.availableDevices?.find(x => x.deviceId === selected);
    this.currentDevice = device || undefined;
  }


  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }


  onTorchCompatible(isCompatible: boolean): void {
    this.torchAvailable$.next(isCompatible || false);
  }

  toggleTorch(): void {
    this.torchEnabled = !this.torchEnabled;
  }

  toggleTryHarder(): void {
    this.tryHarder = !this.tryHarder;
  }

  onCodeResult(resultString: string) {
    console.log(resultString);
    this.qrResultString = resultString;
    //this.currentDevice = undefined;
    this.doLogin(resultString);


  }













}
