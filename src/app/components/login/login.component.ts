import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { AuthenticationService } from '../../services/authentification.service';
import {Constants} from "../../config/constants";
import {AppConfigurationService} from "../../services/app-configuration.service";



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']

})
export class LoginComponent implements OnInit {

  showForm: boolean = this.appConfigService.appConfigValues.loginConfig.SHOW_FORM_LOGIN;
  @ViewChild('expandable') expandable?: ElementRef;

  constructor(private authenticationService: AuthenticationService,
              public appConfigService: AppConfigurationService) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      //this.router.navigate(['/']);
    }
  }

  get constants() {return Constants;}

  ngOnInit() {
    this.authenticationService.logout();

  }

  updateHeight(delay = 0) {
    const el = this.expandable!.nativeElement;
    setTimeout(() => {

      const prevHeight = el.style.height;
      el.style.height = 'auto';
      const newHeight = el.scrollHeight + 'px';
      el.style.height = prevHeight;

      setTimeout(() => {
        el.style.height = newHeight;
      }, 10);
    }, delay);
  }


  toggleLogin() {
    this.showForm = !this.showForm;
    this.updateHeight();
  }


}
