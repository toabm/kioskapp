import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigMenuItemComponent } from './menu-list-item.component';

describe('MenuListItemComponent', () => {
  let component: ConfigMenuItemComponent;
  let fixture: ComponentFixture<ConfigMenuItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigMenuItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
