import {Component, HostBinding, Input, OnInit} from '@angular/core';
import { ConfigMenuItem } from '../../../models/configMenuItem';


// https://careydevelopment.us/blog/how-to-add-a-responsive-sidebar-navigation-menu-to-your-angular-app
@Component({
  selector: 'app-config-menu-item',
  templateUrl: './config-menu-item.component.html',
  styleUrls: ['./config-menu-item.component.scss'],
})
export class ConfigMenuItemComponent implements OnInit {




  // Difference between @HostBinding and @Input
  // @HostBinding, you're binding to the DOM's native property. In other words, aria-expanded is something you'd see on an HTML element that isn't even using the Angular framework.
  // @Input, on the other hand, binds to custom properties you create on-the-fly and reference in your Angular TypeScript code.
  @Input() item!: ConfigMenuItem;


  // The @HostBinding decorator associates a Document Object Model (DOM) property with this TypeScript object.
  // In this case, the code maps the aria-expanded attribute in the DOM to the expanded boolean that you just saw.
  // In other words, if expanded is true on the TypeScript side than aria-expanded is true on the DOM side (or in the HTML).
  @HostBinding('attr.aria-active') ariaActive = this.item ? this.item.active : false;



  constructor() {

  }

  ngOnInit(): void {
  }





}
