import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {ConfigMenuItem} from "../../models/configMenuItem";
import {configMenu} from "../../config/config-menu"
import {AppConfigurationService} from "../../services/app-configuration.service";
import {fadeIn, fadeOut} from "../../animations";
import {AppConfigurationInterface} from "../../models/appConfiguration";
import {AlertService} from "../../services/alert.service";
@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss'],
  animations: [fadeIn, fadeOut]
})
export class ConfigComponent implements OnInit, AfterViewInit {

  public menu: ConfigMenuItem[] = configMenu;
  public changesTimeout?: number;

  // The inner checkbox input
  @ViewChild('locker') public locker: ElementRef | undefined;

  @Output() inputClick = new EventEmitter();

  constructor(public configService: AppConfigurationService, private alertService: AlertService) {}

  ngOnInit(): void {
    // Set first menu item as the active item
    if(this.menu.filter(item => item.active).length === 0) this.menu.filter( item => item.isGroup)[0].active = true;
  }

  ngAfterViewInit(): void {
    // Trigger animation to bring attention to locker button.
    document.querySelectorAll("#locker label")[0].classList.add("wobble");
  }


  /**
   * Will set new active item after waiting for corresponding fadeOut animation to finish.
   * @param item
   */
  onItemSelected( item: ConfigMenuItem){
    this.menu.filter(item => item.isGroup).map(item => item.active = false);
    setTimeout( () => item.active = true, 220);
    (document.querySelectorAll("#locker input")[0] as HTMLInputElement).checked = false;
  }


  onConfigChange(target: any) {
    clearTimeout(this.changesTimeout);
    this.changesTimeout = setTimeout( () => {
      this.saveConfig();
      this.changesTimeout = undefined;
    }, 2000);
  }

  /**
   * Detect if any config property has been changed and sends corresponding request to the server to update the property
   * value on ddbb.
   */
  saveConfig() {

    let isConfigChanged:boolean = false;
    let data: AppConfigurationInterface = <AppConfigurationInterface>{};
    let inputsArray:HTMLInputElement[] = Array.from(document.querySelectorAll(".configInput input, .configInput select"));
    inputsArray.forEach( (input: HTMLInputElement) => {
      let newValue = input.type == "checkbox" ? input.checked : input.value;
      if(String(newValue) !== input.getAttribute('data-old-value') && input.getAttribute('id') != null) {
        isConfigChanged = true;
        // @ts-ignore
        if (!data.hasOwnProperty(input.getAttribute('data-group'))) data[input.getAttribute('data-group')] = {};
        // @ts-ignore
        data[input.getAttribute('data-group')][input.getAttribute('id')] = newValue;
      }
    })
    // Save edited config parameters.
    if(isConfigChanged) {
     console.log("Saving new configuration...")
     this.configService.saveConfigValues(data);
     this.configService.mapConfigValues(data).then(() => this.alertService.success("Los parámetros de configuración fueron actualizados...:)", true));
    }
  }


  lockerChanged(target: any ) {
    let inputsArray:HTMLInputElement[] = Array.from(document.querySelectorAll(".configInput input, .configInput select"));
    inputsArray.forEach( (input: HTMLInputElement) => {
      input.disabled = !target.checked;
    });
  }


  /**
   * This method will trigger #locker animation when a disabled input is clicked, to bring attention over #locker button
   * so user realize this button must be clicked to enable inputs.
   * @param $event
   */
  checkDisabled( $event: Event) {
    let input: HTMLInputElement | HTMLSelectElement = $event.target as HTMLInputElement;

    if ($event.target instanceof HTMLInputElement) { // If click on a "normal" input (type text | number )
      input = $event.target as HTMLInputElement;
    } else if (($event.target as HTMLInputElement).children[0] instanceof HTMLInputElement
    || ($event.target as HTMLElement).classList.contains("checkbox__label") ) { // If click on our angular component <app-checkbox>
      input = ($event.target as HTMLElement).classList.contains("checkbox__label")
        ? ($event.target as HTMLElement).closest("main")!.querySelector("input[type=checkbox]") as HTMLInputElement
        : ($event.target as HTMLInputElement).children[0] as HTMLInputElement;
    } else if ($event.target instanceof HTMLSelectElement) {
      input = $event.target as HTMLSelectElement;
    }
    // Show animation on locker if input is disabled.
    if(input.disabled) {
      document.querySelectorAll("#locker label")[0].classList.remove("wobble");
      setTimeout(() =>  document.querySelectorAll("#locker label")[0].classList.add("wobble"), 10);
    }
  }


}


