import {Observable} from "rxjs";
import {Dimension} from "../models/dimension";
import {Product} from "../models/product";

export interface ProductServiceAPIInterface {
  getProductByModelIdAndDimensions: (modelId: string, selectedDimensions: Dimension[]) => Observable<Product>;
}

