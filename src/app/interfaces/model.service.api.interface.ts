import {Observable} from "rxjs";
import {Model} from "../models/model";
import {Family} from "../models/family";
import {Dimension} from "../models/dimension";

export interface ModelServiceApiInterface {
  getModels: (searchParams: {}) => Observable<Model[]>;
  getFamilies: () => Observable<Family[]>;
  getExistingDimensions(modelId: string | number, dimensions: Dimension[]) : Observable<Dimension[]>
}
