import {Observable} from "rxjs";

export interface CartServiceApiInterface {
  evaluatePromos(): void;
  saveOrder(): Observable<boolean>;
  clearCart(): void;
}
