import { Injectable } from '@angular/core';
import {Observable, tap} from "rxjs";
import {Family} from "../models/family";
import {Model} from "../models/model";
import {ModelServiceApiInterface} from "../interfaces/model.service.api.interface";
import {Dimension} from "../models/dimension";
import {SubFamily} from "../models/subFamily";
import {ApiHttpService} from "../api/api-http.service";
import {ApiEndpointsService} from "../api/api-endpoints.service";
import {HttpParams} from "@angular/common/http";
import {Category} from "../models/category";

@Injectable({
  providedIn: 'root'
})
export class ModelService implements ModelServiceApiInterface {


  constructor(
    // Application Services
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService
  ) { }


  /**
   * I’m using Observable.of here from RxJS to give me an Observable stream from the results, this is a nice way to
   * mimic an Observable response, such as when using Angular’s HttpClient module to return data from an API.
   */
  getModels( searchParams: {} = {}) : Observable<Model[]> {
    return this.apiHttpService.get<Model[]>(this.apiEndpointsService.getModelsEndpoint(),
      { params: new HttpParams({ fromObject: searchParams}) })
  }


  getModelsByCategoryId( catId: string) : Observable<Model[]> {
    return this.apiHttpService.get<Model[]>(this.apiEndpointsService.getModelsByCategoryEndpoint(catId),
      /* { params: new HttpParams({ fromObject: {catId: catId}}) }*/)
  }

  /**
   * For a given model modelId, and a given value (dimComponent) of one or several dimensions, it will return
   * the existing dimComponents for the rest of the model dimensions.
   * @param modelId
   * @param selectedDimensions
   */
  getExistingDimensions(modelId: string, selectedDimensions: Dimension[]) : Observable<Dimension[]> {
    return this.apiHttpService.get<Dimension[]>(this.apiEndpointsService.getExistingDimensionsEndpoint(modelId),
      { params: new HttpParams().set("selectedDimensions", JSON.stringify(selectedDimensions)) });
  }



  getFamilies() : Observable<Family[]> {
    return this.apiHttpService.get<Family[]>(this.apiEndpointsService.getFamiliesEndpoint() )
  }

  getSubFamilies(familyId: string) : Observable<SubFamily[]> {
    return this.apiHttpService.get<SubFamily[]>(this.apiEndpointsService.getSubFamiliesEndpointByFamilyId(familyId))
  }


  getCategories() : Observable<Category[]> {
    return this.apiHttpService.get<Category[]>(this.apiEndpointsService.getCategories()).pipe(
      tap(results => results.sort((a,b) => { return a.orderPriority! - b.orderPriority! }))); // Order by Category.orderPriority
  }
}


