import { Injectable } from '@angular/core';
import {AppConfigurationInterface} from "../models/appConfiguration";
import {ApiHttpService} from "../api/api-http.service";
import {ApiEndpointsService} from "../api/api-endpoints.service";
import {Constants} from "../config/constants";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root' // Since our service is using the new provideIn flag, we don’t need to worry about adding it to a module.
})
export class AppConfigurationService {

  // Init config with DEFAULT CONFIG values
  appConfigValues: AppConfigurationInterface = Constants.defaultConfig;

  constructor(protected apiHttpService: ApiHttpService) {
    this.mapConfigValues(this.getDefaultAppConfiguration());
  }


  /**
   * Get configuration object from database. API_CONFIG_ENDPOINT must be previously configured.
   *
   * The config parameters will be requested to a separate API.
   */
  getServerAppConfiguration(): Observable<AppConfigurationInterface> {
    return this.apiHttpService.get(this.appConfigValues.apiConfig.API_CONFIG_ENDPOINT + "/appConfiguration")
  }

  /**
   * Get configuration object from  Constants.defaultConfig.
   */
  getDefaultAppConfiguration() {
    return Constants.defaultConfig;
  }


  /**
   * This method will save a set of AppConfigurationInterface values to this.appConfigValues. Only the properties contained
   * by receivedConfig that already exists within this.appConfigValues will be saved, any other property will be ignored.
   * @param receivedConfig
   */
  mapConfigValues(receivedConfig: AppConfigurationInterface) {
    return new Promise((resolve, reject) => {
      // Compare 1 by 1 received config properties and set appConfigurationService values.
      for (let [key_1, value_1] of Object.entries(receivedConfig)) {
        if (this.appConfigValues.hasOwnProperty(key_1)) {
          for (let [key_2, value_2] of Object.entries(value_1)) {
            if (this.appConfigValues[key_1].hasOwnProperty(key_2)) this.appConfigValues[key_1][key_2] = receivedConfig[key_1][key_2];
            else console.warn(`No config property found for '${key_1} --> ${key_2}'... please check API docs...`)
          }
        } else console.warn(`No config property group found for '${key_1}'... please check API docs...`)
      }
      resolve(this.appConfigValues)
    });
  }
  // TODO: Save POST api url into api-endpoints.service.ts if possible. Decide where the edited parameter will be saved and if all parameters can be edited.
  saveConfigValues(config: AppConfigurationInterface) {
    this.apiHttpService.post(this.appConfigValues.apiConfig.API_CONFIG_ENDPOINT + "/appConfiguration", config).subscribe();
  }
}


