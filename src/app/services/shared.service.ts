/**
 * Injectable service to communicate components using an RxJS Subject.
 *
 * RxJS Subject is a special type of Observable that allows values to be multicasted to many Observers. While plain
 * Observables are unicast (each subscribed Observer owns an independent execution of the Observable), Subjects are multicast.
 *
 * A Subject is like an Observable, but can multicast to many Observers.
 * Subjects are like EventEmitters: they maintain a registry of many listeners.
 *
 * + Every Subject is an Observable. Given a Subject, you can subscribe to it, providing an Observer, which will start
 * receiving values normally. From the perspective of the Observer, it cannot tell whether the Observable execution is
 * coming from a plain unicast Observable or a Subject.
 *
 * + Every Subject is an Observer. It is an object with the methods next(v), error(e), and complete(). To feed a new
 * value to the Subject, just call next(theValue), and it will be multicasted to the Observers registered to listen to
 * the Subject.
 */
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {NavigationExtras, Router} from "@angular/router";
import {UserIdleService} from "angular-user-idle";
import {Constants} from "../config/constants";
import {AuthenticationService} from "./authentification.service";
import {AppConfigurationService} from "./app-configuration.service";



@Injectable({providedIn: 'root'})
export class SharedService {

  public countDown;
  public showIdleCountDown: boolean = false;


  // Observable string sources
  private emitChangeSource = new Subject<string>();
  // Observable string streams
  changeEmitted = this.emitChangeSource.asObservable();


  constructor(private router: Router,
              private userIdleService: UserIdleService,
              private authenticationService: AuthenticationService,
              private appConfigService: AppConfigurationService) {
    this.countDown = this.appConfigService.appConfigValues.idleTimerConfig.IDLE_LOGOUT;
    // Init idle user timer.
    this.initIdleTimer();
    // Start listening to idle timer.
    this.startIdleTimer()
  }




  /////////////////////////////////////////////////////////////////////////////////////
  // IDLE TIMER ///////////////////////////////////////////////////////////////////////
  //<editor-fold>

  ///////////////////////////////////////////////////////////////////////</editor-fold>
  /////////////////////////////////////////////////////////////////////////////////////







  /////////////////////////////////////////////////////////////////////////////////////
  // IDLE TIMER ///////////////////////////////////////////////////////////////////////
  //<editor-fold>
  initIdleTimer() {

    // Config the Idle Timer
    this.userIdleService.setConfigValues({idle: this.appConfigService.appConfigValues.idleTimerConfig.IDLE_TIMER, timeout: this.appConfigService.appConfigValues.idleTimerConfig.IDLE_LOGOUT, ping: 0})
    this.countDown = this.appConfigService.appConfigValues.idleTimerConfig.IDLE_LOGOUT;
    // Start watching when user idle is starting (user has been away for $idle$ seconds):
    // Fired when timer is starting and return observable (stream) of timer's count.
    this.userIdleService.onTimerStart().subscribe(count => {
      this.countDown--;
      this.showIdleCountDown = true;
    });

    // Start watch when time is up: Fired when time is out and if user did not stop the timer.
    // onTimeout(): Observable<boolean>
    this.userIdleService.onTimeout().subscribe((count) => {
      console.log("IdleTimer Time is Up!! --> Loggin Out...");
      this.stopIdleTimer();
      // Log out user.
      this.authenticationService.logout("welcome");
    });



    // Subscribe to PING event.
    // this.userIdleService.ping$.subscribe(() => console.log("PING"));
  }


  startIdleTimer() {
    this.userIdleService.startWatching();
  }

  stopIdleTimer() {
    this.userIdleService.stopWatching();
    this.userIdleService.stopTimer();
    this.showIdleCountDown = false;
    this.countDown = this.appConfigService.appConfigValues.idleTimerConfig.IDLE_LOGOUT;
  }

  restartIdleTimer() {
    this.userIdleService.stopWatching();
    this.showIdleCountDown = false;
    this.countDown = this.appConfigService.appConfigValues.idleTimerConfig.IDLE_LOGOUT;
    this.userIdleService.resetTimer();
    this.userIdleService.startWatching();
  }
  ///////////////////////////////////////////////////////////////////////</editor-fold>
  /////////////////////////////////////////////////////////////////////////////////////





  /////////////////////////////////////////////////////////////////////////////////////
  // COMMUNICATION EVENTS EMITTER /////////////////////////////////////////////////////
  // <editor-fold>
  // Service message commands
  emitEvent = (change: string) => {this.emitChangeSource.next(change);}

  /**
   *
   * @param pageName
   * @param params
   */
  goToPage(pageName:string, params: any = {}){

    let navigationExtras: NavigationExtras = {state: {params: params}};

    this.router.navigate([`${pageName}`], navigationExtras );
  }



  addMultipleEventListeners(el:Element, s:string, fn: EventListenerOrEventListenerObject) {
    s.split(' ').forEach(e => el.addEventListener(e, fn, false));
  }

  removeMultipleEventListeners(el:Element, s:string, fn: EventListenerOrEventListenerObject) {
    s.split(' ').forEach(e => el.removeEventListener(e, fn, false));
  }
  ///////////////////////////////////////////////////////////////////////</editor-fold>
  /////////////////////////////////////////////////////////////////////////////////////








}





// COMMON METHODS
export const capitalizeFirstLetter = ([ first, ...rest ]:Array<string>, locale = navigator.language) =>
  first.toLocaleUpperCase(locale) + rest.join('')

