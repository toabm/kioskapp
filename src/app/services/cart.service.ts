import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import {SharedService} from "./shared.service";
import {TicketLine} from "../models/ticketLine";
import {Cart} from "../models/cart";
import {Promo} from "../models/promo";
import {CartServiceApiInterface} from "../interfaces/cart.service.api.interface";
import {lastValueFrom, of} from "rxjs";
import {ApiHttpService} from "../api/api-http.service";
import {ApiEndpointsService} from "../api/api-endpoints.service";


@Injectable({providedIn: 'root'})
export class CartService implements Cart, CartServiceApiInterface {

  // Product stored on cart.
  ticketLines: TicketLine[] = [];
  promos?: Promo[];

  constructor(private sharedService: SharedService,
              // Application Services
              private apiHttpService: ApiHttpService,
              private apiEndpointsService: ApiEndpointsService) {
    // TODO: Remove for production. This allows to update the page without loosing the cart.
    if (this.ticketLines.length == 0 && sessionStorage.getItem("cartTicketLines") !== null) this.ticketLines = JSON.parse(<string>sessionStorage.getItem("cartTicketLines")).map((tl:any) => new TicketLine(tl))
  }

  addProductToCart(product: Product, quantity: number = 1) {
    let itemToAdd: TicketLine = new TicketLine({product: product, quantity: quantity});
    this.ticketLines.filter(item => item.product.id === product.id).length === 0
      ? this.ticketLines.push(itemToAdd)
      : this.ticketLines.filter(item => item.product.id === product.id)[0].quantity += quantity;
    // Send the added element to the last position of items array.
    this.ticketLines.splice(this.ticketLines.length - 1,0,this.ticketLines.splice(this.ticketLines.map(line => line.product.id).indexOf(itemToAdd.product.id),1)[0]);
    this.save();
    this.triggerCartChanged();
  }

  removeProduct(prodId: string, evaluatePromos : boolean = false) {
    this.ticketLines.forEach((item, index)=>{
      if(item.product.id == prodId) this.ticketLines.splice(index,1);
    });
    this.save();
    this.triggerCartChanged();
  }

  getTicketLines(): TicketLine[] {return this.ticketLines;}

  clearCart() {

    this.ticketLines = [];
    this.triggerCartChanged();
    sessionStorage.removeItem("cartTicketLines")
    return this.ticketLines;
  }

  save = () => sessionStorage.setItem("cartTicketLines", JSON.stringify(this.ticketLines));

  increaseProduct = (prodId: string) => {
    // @ts-ignore
    this.ticketLines.find(item => item.product.id === prodId)?.quantity = this.ticketLines.find(item => item.product.id === prodId)?.quantity + 1;
    this.save();
    this.triggerCartChanged();
  }

  decreaseProduct = (prodId: string) => {
    if (this.ticketLines.find(item => item.product.id === prodId)?.quantity == 1) this.removeProduct(prodId);
    // @ts-ignore
    else this.ticketLines.find(item => item.product.id === prodId)?.quantity = this.ticketLines.find(item => item.product.id === prodId)?.quantity - 1;
    this.save();
    this.triggerCartChanged();
  }

  setDelivery(prodId: string, checked: boolean) {
    // @ts-ignore
    this.ticketLines.find(item => item.product.id === prodId)?.delivery = checked;
    this.save();
  }

  //TODO: Actualizar modelos para que el pvp total con promos venga calculado del servidor. Actualmente solo vienen los precios de los productos
  // y los descuentos (con impuestos incluidos), y tanto el total de las promos como el total del carrito se calcula desde el front haciendo un
  // sumatorio del precio de cada linea y descontando las promos.
  getTotalPVP() {
    return this.ticketLines != null
      ? this.ticketLines.reduce(function(prev, cur) {return prev + cur.quantity * cur.product.pvp;}, 0) - this.getAppliedPromosAmount()
      : 0;
  }
  getLinePVP(ticketLine: TicketLine): number {
    throw new Error('Method not implemented.');
  }

  getTotalItems(): number {
    return this.ticketLines != null
      ? this.ticketLines.reduce(function(prev, cur) {return prev + cur.quantity;}, 0)
      : 0;
  }

  triggerCartChanged() {
    this.sharedService.emitEvent("cartChanged")
  }



  // API SERVICES
  /**
   * Returns the current Cart with evaluated promotions both on header and ticketLines and updates the Cart instance.
   */
  evaluatePromos =  async (): Promise<boolean> => {
    // console.log("Evaluating promos...")
    let evaluatedCart = await lastValueFrom(this.apiHttpService.post(this.apiEndpointsService.evaluatePromosEndpoint(), {items: this.ticketLines}));
    //  console.log("Cart's been evaluated...")
    // Update cart with promos.
    // Object.assign( this, <Cart><unknown>evaluatedCart); // Object.assign does not type checking so we loose the TicketLine elements.
    this.ticketLines = (<Cart><unknown>evaluatedCart).ticketLines.map((tl:any) => new TicketLine(tl));
    this.promos = (<Cart><unknown>evaluatedCart).promos;
    return evaluatedCart;
  }

  getAppliedPromosNumber = () => {
    return (this.promos?.length || 0) + this.ticketLines.reduce(function(prev, cur) {
      return cur.promos ? prev + cur.promos!.length : 0}, 0);
  }

  getAppliedPromosAmount = () => {
    return (this.promos ? this.promos!.reduce(function(prev, cur) {return prev + cur.amount_vat}, 0) : 0)
      + this.ticketLines.reduce(function(prev, cur)  {
        return cur.promos ? prev + cur.promos!.reduce(function(prev, cur) {return prev + cur.amount_vat}, 0) : 0}, 0) || 0;
  }




  // TODO: Implement SaveOrder
  saveOrder = () => {

    return of(true);
  }

}
