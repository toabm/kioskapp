import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../models/user';
import {ApiEndpointsService} from "../api/api-endpoints.service";
import {AlertService} from "./alert.service";
import {capitalizeFirstLetter} from './shared.service'
import {Router} from "@angular/router";

/**
 * The authentication service is used to login and logout of the application, to login it posts the users credentials
 * to the api and checks the response for a JWT token, if there is one it means authentication was successful so the user
 * details including the token are added to local storage.
 *
 * The logged in user details are stored in local storage so the user will stay logged in if they refresh the browser
 * and also between browser sessions until they logout. If you don't want the user to stay logged in between refreshes
 * or sessions the behaviour could easily be changed by storing user details somewhere less persistent such as session
 * storage which would persist between refreshes but not browser sessions, or in a private variable in the authentication
 * service which would be cleared when the browser is refreshed.
 *
 * There are two properties exposed by the authentication service for accessing the currently logged in user.
 * + The currentUser observable can be used when you want a component to reactively update when a user logs in or out,
 *   for example in the app.component.ts so it can show/hide the main nav bar when the user logs in/out.
 * + The currentUserValue property can be used when you just want to get the current value of the logged in user but don't
 *   need to reactively update when it changes, for example in the auth.guard.ts which restricts access to routes by checking
 *   if the user is currently logged in.
 */
@Injectable({ providedIn: 'root' })
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<User> ;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient, private apiEndpointsService:ApiEndpointsService, private alertService: AlertService, private router: Router) {

    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(<string>localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User | null {
    return this.currentUserSubject.value;
  }

  public get isUserLoggedIn():boolean {
    return this.currentUserSubject.value != null;
  }

  /**
   * Posts the users credentials to the api and checks the response for a JWT token, if there is one it means
   * authentication was successful so the user details including the token are added to local storage.
   * @param username
   * @param password
   */
  login(username: string, password: string) {
    return this.http.post<User>( this.apiEndpointsService.getLoginEndpoint(), { username, password })
      .pipe(map(user => {
        // Store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(<User>user);
        return user;
      }));


  }


  /**
   * Posts the QR Code content to the api and checks the response for a JWT token, if there is one it means
   * authentication was successful so the user details including the token are added to local storage.
   * @param qrContent
   */
  loginQR(qrContent: string):Observable<User> {
    return this.http.post<User>( this.apiEndpointsService.getLoginQREndpoint(), qrContent)
      .pipe(map(user => {
        // Store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(<User>user);
        return user;
      }));


  }

  logout(redirectTo: string | boolean = false):void {
    // Alert user
    if (this.isUserLoggedIn)
      this.alertService.success(`Hasta la próxima ${capitalizeFirstLetter([...this.currentUserValue?.username!])}`, true);
    // remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    // @ts-ignore
    this.currentUserSubject.next(null);
    // Redirect to welcome section
    if (redirectTo) this.router.navigate([redirectTo] );
  }
}
