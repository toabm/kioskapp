import { Injectable } from '@angular/core';
import {ProductServiceAPIInterface} from "../interfaces/product.service.api.interface";
import {Dimension} from "../models/dimension";
import {Observable} from "rxjs";
import {Product} from "../models/product";
import {ApiHttpService} from "../api/api-http.service";
import {ApiEndpointsService} from "../api/api-endpoints.service";
import {HttpParams} from "@angular/common/http";

@Injectable({providedIn: 'root'})
export class ProductService implements ProductServiceAPIInterface {

  constructor(
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService) { }


  /**
   * Following the API docs:
   *
   * @param modelId             Path param => The model ID the product belongs to.
   * @param selectedDimensions  QueryParam => An array of specified dimensions, each one of them with one single dimComponent
   */
  getProductByModelIdAndDimensions(modelId: string, selectedDimensions: Dimension[]): Observable<Product> {
    return this.apiHttpService.get(this.apiEndpointsService.getProductByModelIdAndSelectedDimensionsEndpoint(modelId),
      { params: new HttpParams().set("selectedDimensions", JSON.stringify(selectedDimensions)) });
  }
}
